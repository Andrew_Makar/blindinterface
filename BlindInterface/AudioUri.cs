﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BlindInterface
{
    class AudioUri
    {
        public static Uri StartEntry
        {
            get
            {
                return new Uri(@"resources\start_entry.wav", UriKind.Relative);
            }
        }
        public static Uri MainEntry
        {
            get
            {
                return new Uri(@"resources\main_entry.wav", UriKind.Relative);
            }
        }
        public static Uri BookEntry
        {
            get
            {
                return new Uri(@"resources\books_entry.wav", UriKind.Relative);
            }
        }
        public static Uri ChooseBookEntry1
        {
            get
            {
                return new Uri(@"resources\choose_book_entry_1.wav", UriKind.Relative);
            }
        }
        public static Uri ChooseBookEntry2
        {
            get
            {
                return new Uri(@"resources\choose_book_entry_2.wav", UriKind.Relative);
            }
        }
        public static Uri ReadBookEntry1
        {
            get
            {
                return new Uri(@"resources\read_book_entry_1.wav", UriKind.Relative);
            }
        }
        public static Uri ReadBookEntry2
        {
            get
            {
                return new Uri(@"resources\read_book_entry_2.wav", UriKind.Relative);
            }
        }
        public static Uri PauseEntry
        {
            get
            {
                return new Uri(@"resources\pause_entry.wav", UriKind.Relative);
            }
        }
        public static Uri EndOfBookEntry1
        {
            get
            {
                return new Uri(@"resources\end_of_book_entry_1.wav", UriKind.Relative);
            }
        }
        public static Uri EndOfBookEntry2
        {
            get
            {
                return new Uri(@"resources\end_of_book_entry_2.wav", UriKind.Relative);
            }
        }
        public static Uri NoBooksEntry
        {
            get
            {
                return new Uri(@"resources\no_books_entry.wav", UriKind.Relative);
            }
        }
        public static Uri MissedCallsEntry
        {
            get
            {
                return new Uri(@"resources\missed_calls_entry.wav", UriKind.Relative);
            }
        }
        public static Uri AvailableContactsNoContactsEntry
        {
            get
            {
                return new Uri(@"resources\available_contacts_no_contacts_entry.wav", UriKind.Relative);
            }
        }
        public static Uri AvailableContactsEntry1
        {
            get
            {
                return new Uri(@"resources\available_contacts_entry1.wav", UriKind.Relative);
            }
        }
        public static Uri AvailableContactsEntry2
        {
            get
            {
                return new Uri(@"resources\available_contacts_entry2.wav", UriKind.Relative);
            }
        }
        public static Uri ChooseContactEntry
        {
            get
            {
                return new Uri(@"resources\choose_contact_entry.wav", UriKind.Relative);
            }
        }
        public static Uri CallEntry1
        {
            get
            {
                return new Uri(@"resources\call_entry1.wav", UriKind.Relative);
            }
        }
        public static Uri CallEntry2
        {
            get
            {
                return new Uri(@"resources\call_entry2.wav", UriKind.Relative);
            }
        }
        public static Uri CallFinishedEntry
        {
            get
            {
                return new Uri(@"resources\call_finished_entry.wav", UriKind.Relative);
            }
        }
        public static Uri CallDeniedEntry
        {
            get
            {
                return new Uri(@"resources\call_denied_entry.wav", UriKind.Relative);
            }
        }
        public static Uri IncomingCallEntry1
        {
            get
            {
                return new Uri(@"resources\incoming_call_entry1.wav", UriKind.Relative);
            }
        }
        public static Uri IncomingCallEntry2
        {
            get
            {
                return new Uri(@"resources\incoming_call_entry2.wav", UriKind.Relative);
            }
        }
        public static Uri EndIncomingCallEntry
        {
            get
            {
                return new Uri(@"resources\end_incoming_call_entry.wav", UriKind.Relative);
            }
        }
        public static Uri DenyIncomingCallEntry
        {
            get
            {
                return new Uri(@"resources\deny_incoming_call_entry.wav", UriKind.Relative);
            }
        }
        public static Uri ContactFinishedIncomingCallEntry
        {
            get
            {
                return new Uri(@"resources\contact_finished_incoming_call_entry.wav", UriKind.Relative);
            }
        }

    }
}
