﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BlindInterface.Actions
{
    abstract class SkypeCallAction : Action
    {
        public override void Do()
        {
            base.Do();
            Action.currentActions.Add(this);
            isOn = true;

            Skype.CallFinished += Skype_CallFinished;
            Skype.CallRefused += Skype_CallRefused;
            Skype.CallMissed += Skype_CallMissed;
        }

        void Skype_CallMissed(object sender, EventArgs e)
        {
            if (!isOn)
            {
                return;
            }
            isOn = false;
            base.OnActionEnded();
        }
        void Skype_CallRefused(object sender, EventArgs e)
        {
            if (!isOn)
            {
                return;
            }
            isOn = false;
            base.OnActionEnded();
        }
        void Skype_CallFinished(object sender, EventArgs e)
        {
            if (!isOn)
            {
                return;
            }
            isOn = false;
            base.OnActionEnded();
        }

        public override void ForceStop()
        {
            base.ForceStop();

            if (!isOn)
            {
                return;
            }
            isOn = false;

            Skype.CallFinished -= Skype_CallFinished;
            Skype.CallRefused -= Skype_CallRefused;

           // Skype.FinishCall();
        }
    }
}
