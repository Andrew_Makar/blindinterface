using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace SkypeControl
{
    internal partial class SkypeClient : Form
    {

        public static IntPtr hhh;

        public SkypeClient()
        {
            InitializeComponent();

            UM_SkypeControlAPIDiscover = Platform.RegisterWindowMessage(Constants.SkypeControlAPIDiscover);
            UM_SkypeControlAPIAttach = Platform.RegisterWindowMessage(Constants.SkypeControlAPIAttach);

            CreateHandle();
            hhh = this.Handle;
        }

        public bool Connect()
        {
            IntPtr result;
            IntPtr aRetVal = Platform.SendMessageTimeout(Platform.HWND_BROADCAST, UM_SkypeControlAPIDiscover, Handle, IntPtr.Zero, Platform.SendMessageTimeoutFlags.SMTO_NORMAL, 100, out result);

            return (aRetVal != IntPtr.Zero);
        }

        public void Disconnect()
        {
            Command("");
            mySkypeHandle = IntPtr.Zero;
        }

        public bool Command(string theCommand)
        {
            Platform.CopyDataStruct aCDS = new Platform.CopyDataStruct();

            aCDS.ID = "1";
            aCDS.Data = theCommand;
            aCDS.Length = aCDS.Data.Length + 1;

            IntPtr result = new IntPtr();
            //IntPtr aRetVal = Platform.SendMessageTimeout(mySkypeHandle, 
            //    Platform.WM_COPYDATA, Handle, ref aCDS, Platform.SendMessageTimeoutFlags.SMTO_NORMAL, 100, out result);
            IntPtr mySkypeHandle2 = mySkypeHandle;
         /*   foreach (var p in typeof(SkypeClient).GetProperties())
            {
                if (p.Name == "Handle")
                {
                    int k = 0;
                  //  p.GetValue(this,
                }
            }

            IntPtr hWnd = IntPtr.Zero;

            foreach (Process pList in Process.GetProcesses())
                if (pList.MainWindowTitle.Contains(wName))
                    hWnd = pList.MainWindowHandle;*/
            IntPtr Handle2 = hhh;
            //IntPtr Handle2 = (IntPtr)typeof(Control).GetProperty("Handle").GetValue(this, null);
            //var Handle2 = typeof(SkypeClient).InvokeMember("Handle", System.Reflection.BindingFlags.GetProperty, null, this, null);
            //IntPtr Handle2 = this.Handle;
            

            object[] parameters = new object[] { mySkypeHandle2, (UInt32)Platform.WM_COPYDATA, Handle2, aCDS, Platform.SendMessageTimeoutFlags.SMTO_NORMAL, (UInt32)100, result };

            //IntPtr aRetVal = (IntPtr)typeof(Platform).GetMethod("SendMessageTimeout").Invoke(null, parameters);
            IntPtr aRetVal = IntPtr.Zero;
            

            foreach (var method in typeof(Platform).GetMethods())
            {
                if (method.Name == "SendMessageTimeout" && method.GetParameters().Length == parameters.Length)
                {
                    if (method.GetParameters()[3].ParameterType == typeof(IntPtr))
                    {
                        continue;
                    }
                    aRetVal = (IntPtr)method.Invoke(null, parameters);
                    break;
                }
            }
            
            return (aRetVal != IntPtr.Zero);
        }

        private UInt32 UM_SkypeControlAPIDiscover = 0;
        private UInt32 UM_SkypeControlAPIAttach = 0;

        private IntPtr mySkypeHandle = IntPtr.Zero;

        public event SkypeAttachHandler SkypeAttach;
        public event SkypeResponseHandler SkypeResponse;

        protected override void WndProc(ref Message m)
        {
            UInt32 aMsg = (UInt32)m.Msg;

            if (aMsg == UM_SkypeControlAPIAttach)
            {
                SkypeAttachStatus anAttachStatus = (SkypeAttachStatus)m.LParam;

                if (anAttachStatus == SkypeAttachStatus.Success)
                    mySkypeHandle = m.WParam;

                if (SkypeAttach != null)
                    SkypeAttach(this, new SkypeAttachEventArgs(anAttachStatus));

                m.Result = new IntPtr(1);
                return;
            }

            if (aMsg == Platform.WM_COPYDATA)
            {
                if (m.WParam == mySkypeHandle)
                {
                    Platform.CopyDataStruct aCDS = (Platform.CopyDataStruct)m.GetLParam(typeof(Platform.CopyDataStruct));
                    string aResponse = aCDS.Data;

                    if (SkypeResponse != null)
                        SkypeResponse(this, new SkypeResponseEventArgs(aResponse));

                    m.Result = new IntPtr(1);
                    return;
                }
            }

            base.WndProc(ref m);
        }

    }
}