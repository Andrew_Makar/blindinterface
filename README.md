This application is created for visually impaired and blind people to have an ability to use their personal computer.

Application provides friendly interface for its users people:
 --- Application shares information with user using voice commands.
 --- User controls program using mouse. User clicks either left or right mouse button to perform one of two proposed choices.

Currently application has two features: reading audio books and performing/receiving Skype calls.

To read audio books they should be preliminary added to the program by a manager program that is a part of this project. Manager program provides simple interface to add/remove books to application and to record voice tips for books. In order to run manager program you should have a microphone.

To perform/receive Skype calls, Skype program should be installed on the computer and user should be logged in with his Skype account. Application uses Skype API to communicate with Skype.

Language of the interface: Russian.