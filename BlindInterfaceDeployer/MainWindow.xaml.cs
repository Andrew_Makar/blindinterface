﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Forms;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Microsoft.Win32;

namespace BlindInterfaceDeployer
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            FolderBrowserDialog dialog = new FolderBrowserDialog();
            if (dialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                pathTextBox.Text = dialog.SelectedPath;
            }
        }

        private void Button_Click_2(object sender, RoutedEventArgs e)
        {
            string path = pathTextBox.Text;
            if (path[path.Length - 1] != '\\') path += '\\';
            path += "BlindInterface\\";

            if (System.IO.Directory.Exists(path))
            {
                System.Windows.MessageBox.Show("По вибраному шляху не повинно існувати папки BlindInterface. Видаліть її, або змініть шлях.", "Помилка", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }

            System.IO.Directory.CreateDirectory(path);
            System.IO.Directory.CreateDirectory(path + "books\\");
            System.IO.Directory.CreateDirectory(path + "titles\\");
            System.IO.Directory.CreateDirectory(path + "resources\\");
            System.IO.Directory.CreateDirectory(path + "skype\\");
            
            IEnumerable<string> resources = System.IO.Directory.EnumerateFiles("resources\\resources\\");
            foreach (var r in resources)
            {
                string s = "";
                for (int i = r.Length-1; r[i] != '\\'; i--)
                {
                    s = r[i] + s;
                }

                System.IO.File.Copy(r, path + "resources\\" + s);
            }

            System.IO.File.Copy("resources\\nAudio.dll", path + "NAudio.dll");
            System.IO.File.Copy("resources\\manager.exe", path + "manager.exe");
            System.IO.File.Copy("resources\\program.exe", path + "program.exe");
            System.IO.File.Copy("resources\\MyTestApp.exe", path + "MyTestApp.exe");
            System.IO.File.Copy("resources\\SkypeControl.dll", path + "SkypeControl.dll");

            System.IO.File.Create(path + "books.txt");
            System.IO.File.Create(path + "skype.txt");

            RegistryKey add = Registry.CurrentUser.OpenSubKey("SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Run", true);
            add.SetValue("BlindInterface", "\"" + path + "program.exe" + "\"");

            System.Windows.MessageBox.Show("Встановлення успішно завершене!", "Успіх", MessageBoxButton.OK, MessageBoxImage.Asterisk);
        }
    }
}
