﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SkypeManager.SkypeResponces
{
    class SearchCallsResponce : SkypeResponce
    {
        List<int> callsId;
        public List<int> CallsIds
        {
            get
            {
                return callsId;
            }
        }

        public SearchCallsResponce(List<int> callsId)
        {
            this.callsId = callsId;
        }

        public static bool TryParse(string responce, out SkypeResponce result)
        {
            result = null;

            if (responce == "CALLS ")
            {
                result = new SearchCallsResponce(new List<int>());
                return true;
            }

            string[] tokens = responce.Split(' ');
            if (tokens.Length == 0)
            {
                return false;
            }

            if (tokens[0] != "CALLS")
            {
                return false;
            }


            List<int> callIds = new List<int>();

            for (int i = 1; i < tokens.Length; i++)
            {
                string s = tokens[i].Substring(0, tokens[i].Length - ((i == tokens.Length - 1) ? 0 : 1));
                int id;
                if (int.TryParse(s, out id))
                {
                    callIds.Add(id);
                }
                else
                {
                    return false;
                }
            }

            result = new SearchCallsResponce(callIds);

            return true;
        }
    }
}
