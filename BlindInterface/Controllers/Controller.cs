﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using BlindInterface.Actions;
using Action = BlindInterface.Actions.Action;

namespace BlindInterface.Controllers
{
    public class Controller
    {
        Action action1;
        Action action2;
        
        public Controller(Action action1, Action action2)
        {
            this.action1 = action1;
            this.action2 = action2;
        }

        public void Action1()
        {
            Logger.Log("Controller Action 1");

            this.action1.Do();
        }
        public void Action2()
        {
            Logger.Log("Controller Action 2");

            this.action2.Do();
        }
    }
}
