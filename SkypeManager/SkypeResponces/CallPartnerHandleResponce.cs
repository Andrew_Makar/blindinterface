﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SkypeManager.SkypeResponces
{
    class CallPartnerHandleResponce : SkypeResponce
    {
        int id;
        string partnerHandle;
        public int Id
        {
            get
            {
                return id;
            }
        }
        public string PartnerHandle
        {
            get
            {
                return partnerHandle;
            }
        }

        public CallPartnerHandleResponce(int id, string partnerHandle)
        {
            this.id = id;
            this.partnerHandle = partnerHandle;
        }

        public static bool TryParse(string responce, out SkypeResponce result)
        {
            result = null;

            string[] tokens = responce.Split(' ');
            if (tokens.Length != 4)
            {
                return false;
            }

            if (tokens[0] != "CALL")
            {
                return false;
            }
            int id;
            if (!int.TryParse(tokens[1], out id))
            {
                return false;
            }

            if (tokens[2] != "PARTNER_HANDLE")
            {
                return false;
            }

            string partnerHandle = tokens[3];

            result = new CallPartnerHandleResponce(id, partnerHandle);

            return true;
        }
    }
}
