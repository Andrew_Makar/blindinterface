﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using NAudio;
using NAudio.Mixer;
using NAudio.Wave;

namespace BlindInterfaceManager
{
    /// <summary>
    /// Interaction logic for SkypeControl.xaml
    /// </summary>
    public partial class SkypeControl : UserControl
    {
        WaveIn waveIn;
        UnsignedMixerControl volumeControl;
        WaveFormat format;
        WaveFileWriter writer;

        public ViewBooksControl ViewBooks
        {
            get;
            set;
        }

        public SkypeControl()
        {
            InitializeComponent();

            grid.DataContext = this;


            var mixerLine = new MixerLine((IntPtr)0, 0, MixerFlags.WaveIn);
            foreach (var control in mixerLine.Controls)
            {
                if (control.ControlType == MixerControlType.Volume)
                {
                    volumeControl = control as UnsignedMixerControl;
                    break;
                }
            }

            waveIn = new WaveIn();
            waveIn.DeviceNumber = 0;
            waveIn.DataAvailable += waveIn_DataAvailable;
            int sampleRate = 8000;
            int channels = 1;
            format = new WaveFormat(sampleRate, channels);
            waveIn.WaveFormat = format;
            waveIn.StartRecording();

            FileToListbox();
        }

        int cnt = 0;
        double m;

        void waveIn_DataAvailable(object sender, WaveInEventArgs e)
        {
            if (isRecording)
            {
                writer.Write(e.Buffer, 0, e.BytesRecorded);
            }

            for (int i = 0; i < e.BytesRecorded; i += 2)
            {
                short s = (short)((e.Buffer[i + 1] << 8) | e.Buffer[i]);
                double s2 = s / (double)(1 << 15);
                cnt++;
                m = Math.Max(m, s2);
                if (cnt >= 100)
                {
                    cnt = 0;
                    progressBar.Value = m * 100;
                    m = 0;
                }
            }
        }

        private void Slider_ValueChanged_1(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            volumeControl.Percent = e.NewValue;
        }

        bool isRecording = false;

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            if (!isRecording)
            {
                writer = new WaveFileWriter(System.AppDomain.CurrentDomain.BaseDirectory + "temp.wav", format);
                isRecording = true;
                record.Content = "Зупинити запис";
                listen.IsEnabled = false;
                add.IsEnabled = false;
                //open.IsEnabled = false;
                //bookName.IsEnabled = false;
                //path.IsEnabled = false;
            }
            else
            {
                isRecording = false;
                writer.Close();
                record.Content = "Записати ще раз";
                listen.IsEnabled = true;
                add.IsEnabled = true;
                //open.IsEnabled = true;
                //bookName.IsEnabled = true;
                //path.IsEnabled = true;
            }
        }
        MediaPlayer player;
        private void listen_Click_1(object sender, RoutedEventArgs e)
        {
            listen.IsEnabled = false;
            record.IsEnabled = false;
            player = new MediaPlayer();
            player.MediaEnded += player_MediaEnded;
            player.MediaFailed += player_MediaEnded;
            player.Open(new Uri(System.AppDomain.CurrentDomain.BaseDirectory + "temp.wav", UriKind.Relative));
            player.Play();
        }

        void player_MediaEnded(object sender, EventArgs e)
        {
            player.Close();
            listen.IsEnabled = true;
            record.IsEnabled = true;
        }




        private void Button_Click_2(object sender, RoutedEventArgs e)
        {
            

            //Debugger.Break();
            string baseDirectory = AppDomain.CurrentDomain.BaseDirectory;
            string filePath = baseDirectory + @"skype\" + skypeId.Text;
            string titleFileName = skypeId.Text+".wav";
            
            string titlePath = baseDirectory + @"skype\" + titleFileName;
            if (System.IO.File.Exists(titlePath))
            {
                System.IO.File.Delete(titlePath);
            }
            System.IO.File.Copy(System.AppDomain.CurrentDomain.BaseDirectory + "temp.wav", titlePath);

            System.IO.File.AppendAllText(System.AppDomain.CurrentDomain.BaseDirectory + "skype.txt", "\r\n"+ skypeId.Text);
            FileToListbox();

            MessageBox.Show("Контакт успішно добавлено", "Успіх", MessageBoxButton.OK, MessageBoxImage.Asterisk);
        }

        private void removeButton_Click_1(object sender, RoutedEventArgs e)
        {
            if (listbox.SelectedIndex == -1)
            {
                return;
            }

            listbox.Items.RemoveAt(listbox.SelectedIndex);
            ListboxToFile();
        }

        private void FileToListbox()
        {
            string[] s = System.IO.File.ReadAllLines(System.AppDomain.CurrentDomain.BaseDirectory + "skype.txt");
            listbox.Items.Clear();
            foreach (var t in s)
            {
                listbox.Items.Add(t);
            }
        }
        private void ListboxToFile()
        {
            string s = "";
            foreach (var t in listbox.Items)
            {
                s += (string)t;
                s += "\r\n";
            }
            s = s.Remove(s.Length - 2);
            System.IO.File.WriteAllText(System.AppDomain.CurrentDomain.BaseDirectory + "skype.txt", s);
        }

    }
}
