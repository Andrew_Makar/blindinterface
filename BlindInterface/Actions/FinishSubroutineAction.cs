﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BlindInterface.Actions
{
    class FinishSubroutineAction : Action
    {
        public FinishSubroutineAction()
        {
            this.name = "Finish subroutine action";
        }
        public override void Do()
        {
            Action action = ActionFactory.CreateRedirectionAction(StateManager.PopStack());
            action.ActionEnded += action_ActionEnded;
            action.Do();
        }
        public override void ForceStop()
        {
            base.ForceStop();
            if (ParentAction != null)
            {
                ParentAction.ForceStop();
            }
        }

        void action_ActionEnded()
        {
            base.OnActionEnded();
        }
    }
}
