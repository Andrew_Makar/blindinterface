﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SkypeManager.SkypeResponces
{
    class CallUnknownResponce : CallStatusResponce
    {
        public CallUnknownResponce(int id)
        {
            this.id = id;
            this.status = null;
        }

        public static bool TryParse(string responce, out SkypeResponce result)
        {
            return CallStatusResponce.TryParse(responce, null, out result, typeof(CallUnknownResponce));
        }
    }
}
