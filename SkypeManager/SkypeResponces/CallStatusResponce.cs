﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SkypeManager.SkypeResponces
{
    class CallStatusResponce : SkypeResponce
    {
        protected int id;
        protected string status;
        protected CallStatus callStatus;
        public string Status
        {
            get
            {
                return status;
            }
        }
        public int Id
        {
            get
            {
                return id;
            }
        }
        public CallStatus CallStatus
        {
            get
            {
                return callStatus;
            }
        }

        protected static bool TryParse(string responce, string status, out SkypeResponce result, Type resultType)
        {
            result = null;

            string[] tokens = responce.Split(' ');

            if (tokens.Length != 4)
            {
                return false;
            }

            if (tokens[0] != "CALL")
            {
                return false;
            }
            int id;
            if (!int.TryParse(tokens[1], out id))
            {
                return false;
            }

            if (status != null && tokens[2] != "STATUS")
            {
                return false;
            }
            if (status != null && tokens[3] != status)
            {
                return false;
            }

            result = (SkypeResponce)Activator.CreateInstance(resultType, new object[] { id });

            return true;
        }
    }
}
