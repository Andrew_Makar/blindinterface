﻿using SkypeControl;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

using SkypeManager.SkypeResponces;
using System.Threading;

namespace SkypeManager
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        Manager manager;

        public MainWindow()
        {
            InitializeComponent();
            manager = new Manager();

            manager.CallFinished += manager_CallFinished;
            manager.CallRefused += manager_CallRefused;
            manager.CallMissed += manager_CallMissed;
            manager.IncomingCall += manager_IncomingCall;

            /*commandResponces = new Dictionary<string, CommandResponce>();
            callId = -1;

            skype = new SkypeProxy();
            skype.Conect();
            skype.SkypeResponse += skype_SkypeResponse;*/
        }

        void manager_IncomingCall(object sender, string partnerId)
        {
            MessageBox.Show("Incoming call from " + partnerId);
        }

        void manager_CallMissed(object sender, EventArgs e)
        {
            MessageBox.Show("Call missed");
        }

        void manager_CallRefused(object sender, EventArgs e)
        {
            MessageBox.Show("Call refused");
        }

        void manager_CallFinished(object sender, EventArgs e)
        {
            MessageBox.Show("Call finished");
        }


        private void callButton_Click_1(object sender, RoutedEventArgs e)
        {
            manager.Call(idTextBox.Text);
        }

        private void EndCall_Click_1(object sender, RoutedEventArgs e)
        {
            manager.FinishCall();
        }

        private void AnswerCall_Click_1(object sender, RoutedEventArgs e)
        {
            //manager.AnswerCall();
            manager.ResetMissedCalls();
        }

        private void DenyCall_Click_1(object sender, RoutedEventArgs e)
        {
            //manager.DenyCall();
            var t = manager.GetMissedCalls();
            string s = "";
            foreach (var tt in t)
            {
                if (tt.Value) s += tt.Key + "\n";
            }

            MessageBox.Show(s);


        }

        private void GetOnlineContacts_Click_1(object sender, RoutedEventArgs e)
        {
            /*List<string> contacts = manager.GetOnlineContacts();
            string res = "";
            foreach (var c in contacts)
            {
                res += c + "\n";
            }
            MessageBox.Show(res);*/
        }
    }
}
