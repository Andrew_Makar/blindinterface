﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Action = BlindInterface.Actions.Action;
using BlindInterface.Controllers;

namespace BlindInterface.States
{
    class StateCreator : State
    {
        public StateCreator(string name) : base(null, null, null, null, name) { }
        public Action EnterAction
        {
            get
            {
                return enterAction;
            }
            set
            {
                enterAction = value;
            }
        }
        public Action DoAction
        {
            get
            {
                return doAction;
            }
            set
            {
                doAction = value;
            }
        }
        public Action LeaveAction
        {
            get
            {
                return leaveAction;
            }
            set
            {
                leaveAction = value;
            }
        }
        public Controller Controller
        {
            get
            {
                return controller;
            }
            set
            {
                controller = value;
            }
        }
        public State AfterInterruptionState
        {
            get
            {
                return this.afterInterruptionState;
            }
            set
            {
                this.afterInterruptionState = value;
            }
        }
        public bool IsInterruptable
        {
            get
            {
                return isInterruptable;
            }
            set
            {
                isInterruptable = value;
            }
        }
    }
}
