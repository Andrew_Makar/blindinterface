﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BlindInterface.Actions
{
    class AudioBookAction : AudioAction
    {
        int index;
        public AudioBookAction(Uri uri, double progress, int index)
            : base(uri, progress)
        {
            this.index = index;

            this.name = "Audio book action";
        }

        private void SaveProgress()
        {
            progress = AudioManager.Progress;
            BookProvider.SetBookProgress(index, progress);
        }

        public override void Do()
        {

            progress = BookProvider.GetBookProgress(index);
            base.Do();
        }

        protected override void AudioManager_MediaEndedEvent()
        {
            if (isOn)
            {
                SaveProgress();
            }
            base.AudioManager_MediaEndedEvent();
        }

        public override void ForceStop()
        {
            if (isOn)
            {
                SaveProgress();
            }
            base.ForceStop();
        }
    }
}
