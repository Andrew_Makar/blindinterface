﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BlindInterface.Actions
{
    class ConditionAction : Action
    {
        Func<bool> condition;
        Action ifTrue;
        Action ifFalse;
        Action toDo;

        public ConditionAction(Func<bool> condition, Action ifTrue, Action ifFalse)
        {
            this.condition = condition;
            this.ifTrue = ifTrue;
            this.ifFalse = ifFalse;
            ifTrue.ParentAction = this;
            ifTrue.ParentAction = this;
            this.name = "Condition action";
        }

        public override void Do()
        {
            base.Do();
            Action.currentActions.Add(this);
            isOn = true;

            toDo = null;

            if (condition())
            {
                toDo = ifTrue;
            }
            else
            {
                toDo = ifFalse;
            }

            toDo.ActionEnded += toDo_ActionEnded;
            toDo.Do();
        }

        void toDo_ActionEnded()
        {
            if (toDo != null)
            {
                toDo.ActionEnded -= toDo_ActionEnded;
            }
            base.OnActionEnded();
            isOn = false;
        }

        public override void ForceStop()
        {
            base.ForceStop();

            if (!isOn)
            {
                return;
            }

            if (toDo != null)
            {
                toDo.ActionEnded -= toDo_ActionEnded;
            }

            isOn = false;
            if (ParentAction != null)
            {
                ParentAction.ForceStop();
            }

            toDo.ForceStop();
        }
    }
}
