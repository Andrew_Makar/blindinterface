﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BlindInterface.Interrupters
{
    class InterrupterFactory
    {
        public static Interrupter CreateCallInterrupter(int index)
        {
            return new IncomingCallInterrupter(index);
        }
        public static List<Interrupter> CreateCallInterrupters()
        {
            List<Interrupter> result = new List<Interrupter>();
            for (int i = 0; i < Skype.GetAllContacts().Count; i++)
            {
                result.Add(InterrupterFactory.CreateCallInterrupter(i));
            }
            return result;
        }
    }
}
