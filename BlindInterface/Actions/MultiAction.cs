﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;

namespace BlindInterface.Actions
{
    class MultiAction : Action
    {
        List<Action> actions;

        public MultiAction(List<Action> actions)
        {
            this.actions = actions;
            for (int i = 0; i < actions.Count; i++)
            {
                actions[i].ParentAction = this;
            }
            this.name = "Multi action";
        }

        int currentActionIndex;

        public override void Do()
        {
            base.Do();

            isOn = true;
            Action.currentActions.Add(this);

            Debug.WriteLine("Multi action started");
            currentActionIndex = 0;
            actions[0].ActionEnded += MultiAction_ActionEnded;
            Logger.Log("Multi action subaction started");
            actions[0].Do();
        }

        void MultiAction_ActionEnded()
        {
            Logger.Log("Multi action subaction ended");
            actions[currentActionIndex].ActionEnded -= MultiAction_ActionEnded;
            currentActionIndex++;
            if (currentActionIndex >= actions.Count)
            {
                isOn = false;

                Debug.WriteLine("Multi action ended");
                base.OnActionEnded();
                return;
            }
            Logger.Log("Multi action subaction started");
            actions[currentActionIndex].ActionEnded+=MultiAction_ActionEnded;
            actions[currentActionIndex].Do();
        }
        public override void ForceStop()
        {
            if (!isOn)
            {
                return;
            }

            base.ForceStop();
            isOn = false;



            actions[currentActionIndex].ActionEnded -= MultiAction_ActionEnded;
            actions[currentActionIndex].ForceStop();

            Debug.WriteLine("Multi action force stopped");
            if (this.ParentAction != null)
            {
                this.ParentAction.ForceStop();
            }
        }
    }
}
