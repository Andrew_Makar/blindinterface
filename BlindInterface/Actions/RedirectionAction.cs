﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using BlindInterface.States;

namespace BlindInterface.Actions
{
    class RedirectionAction : Action
    {
        State target;

        public RedirectionAction(State target)
        {
            this.target = target;
            this.name = "Redirection action";
        }

        public override void Do()
        {
            base.Do();

            for (int i = 0; i < Action.currentActions.Count; i++)
            {
                int cnt = Action.currentActions.Count;
                Action.currentActions[i].ForceStop();
                if (Action.currentActions.Count != cnt)
                {
                    i--;
                }
            }
            Action.currentActions.Clear();

           // if (AudioManager.PlayingAction != null)
           // {
           //     AudioManager.PlayingAction.ForceStop();
           // }

            StateManager.Redirect(target);
            base.OnActionEnded();
        }
        public override void ForceStop()
        {
            base.ForceStop();

            if (this.ParentAction != null)
            {
                this.ParentAction.ForceStop();
            }            
        }
    }
}
