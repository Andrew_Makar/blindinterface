﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SkypeManager.SkypeManagerStates
{
    public interface ISkypeManagerState
    {
        void Enter();
        void Leave();

        void Call(string skypeId);
        void FinishCall();
        void AnswerCall();
        void DenyCall();
    }
}
