﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BlindInterface.Actions
{
    class EmptyAction : Action
    {
        public EmptyAction()
        {
            this.name = "Empty action";
        }

        public override void Do()
        {
            base.Do();

            base.OnActionEnded();
        }
        public override void ForceStop()
        {
            base.ForceStop();

            if (this.ParentAction != null)
            {
                this.ParentAction.ForceStop();
            }
        }
    }
}
