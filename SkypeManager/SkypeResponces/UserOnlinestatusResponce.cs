﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SkypeManager.SkypeResponces
{
    class UserOnlinestatusResponce : SkypeResponce
    {
        string handle;
        SkypeOnlineStatus status;
        public string Handle
        {
            get
            {
                return handle;
            }
        }
        public SkypeOnlineStatus Status
        {
            get
            {
                return status;
            }
        }

        public UserOnlinestatusResponce(string handle, SkypeOnlineStatus status)
        {
            this.handle = handle;
            this.status = status;
        }

        public static bool TryParse(string responce, out SkypeResponce result)
        {
            result = null;

            string[] tokens = responce.Split(' ');
            if (tokens.Length != 4)
            {
                return false;
            }
            if (tokens[0] != "USER")
            {
                return false;
            }
            string handle = tokens[1];
            if (tokens[2] != "ONLINESTATUS")
            {
                return false;
            }

            SkypeOnlineStatus status;

            switch (tokens[3])
            {
                case "ONLINE":
                    status = SkypeOnlineStatus.Online;
                    break;
                case "OFFLINE":
                    status = SkypeOnlineStatus.Offline;
                    break;
                case "AWAY":
                    status = SkypeOnlineStatus.Away;
                    break;
                case "DND":
                    status = SkypeOnlineStatus.Dnd;
                    break;
                default:
                    return false;
            }

            result = new UserOnlinestatusResponce(handle, status);
            return true;
        }
    }
}
