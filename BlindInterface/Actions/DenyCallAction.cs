﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BlindInterface.Actions
{
    class DenyCallAction : Action
    {
        public DenyCallAction()
        {
            this.name = "Deny call action";
        }

        public override void Do()
        {
            base.Do();

            Skype.DenyCall();

            base.OnActionEnded();
        }
        public override void ForceStop()
        {
            base.ForceStop();
            if (this.ParentAction != null)
            {
                this.ParentAction.ForceStop();
            }
        }
    }
}
