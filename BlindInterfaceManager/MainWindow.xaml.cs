﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using NAudio;
using NAudio.Mixer;
using NAudio.Wave;

namespace BlindInterfaceManager
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            while (WaveIn.DeviceCount == 0)
            {
                if (MessageBox.Show("Мікрофон не підключено. Підключіть, бідь ласка мікрофон. Він потрібний для функціонування прогарми.", "Помилка", MessageBoxButton.OKCancel, MessageBoxImage.Error) == MessageBoxResult.Cancel)
                {
                    return;
                }
            }
            InitializeComponent();

            addBooks.ViewBooks = viewBooks;



        }

        bool forceClose = false;

        private void Window_Closing_1(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (!forceClose)
            {
                e.Cancel = true;
                this.WindowState = System.Windows.WindowState.Minimized;
                forceClose = false;
            }
            viewBooks.Save();
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            forceClose = true;
            Process.Start(System.AppDomain.CurrentDomain.BaseDirectory + "program.exe");
            this.Close();
        }
    }
}
