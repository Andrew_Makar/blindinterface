﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BlindInterface.States;

namespace BlindInterface.Interrupters
{
    class IncomingCallInterrupter : Interrupter
    {
        string skypeId;
        int index;

        public IncomingCallInterrupter(int index)
        {
            this.index = index;
            this.skypeId = Skype.GetAllContacts()[index];

            base.startState = StateFactory.CreateIncomingCallState(index);

            Skype.IncomingCall += Skype_IncomingCall;
        }

        void Skype_IncomingCall(object sender, string skypeId)
        {
            if (skypeId == this.skypeId)
            {
                base.OnInterruption();
            }
        }

        public override void DenyInterruption()
        {
            Skype.DenyCall();
        }
    }
}
