﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BlindInterface.Actions
{
    class TextAction : Action
    {
        string message;
        public TextAction(string message)
        {
            this.message = message;
            this.name = "Text action";
        }

        public override void Do()
        {
            base.Do();

            TextManager.Message(message);
            base.OnActionEnded();
        }

        public override void ForceStop()
        {
            base.ForceStop();
            if (this.ParentAction != null)
            {
                this.ParentAction.ForceStop();
            }            
        }
    }
}
