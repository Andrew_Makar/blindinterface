﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BlindInterface.Actions
{
    class AnswerCallAction : SkypeCallAction
    {
        public AnswerCallAction()
        {
            this.name = "Answer call action";
        }

        public override void Do()
        {
            base.Do();
            Skype.AnswerCall();
        }
    }
}
