﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using BlindInterface.States;
using BlindInterface.Actions;
using BlindInterface.Interrupters;

namespace BlindInterface
{
    class StateManager
    {
        static State currentState;
        public static State CurrentState
        {
            get
            {
                return currentState;
            }
        }

        public static void Redirect(State state)
        {
            Logger.Log(String.Format("Redirecting to state \"{0}\"", state.Name));

            if (currentState != null)
            {
                currentState.Leave();
            }
            currentState = state;
            currentState.Enter();

            Logger.Log(String.Format("Redirected to state \"{0}\"", state.Name));
        }

        static Stack<State> stateStack = new Stack<State>();

        public static State PopStack()
        {
            State res = stateStack.Pop();
            return res;
        }

        public static void AddInterrupter(Interrupter interrupter)
        {
            interrupter.Interruption += interrupter_Interruption;
        }

        static void interrupter_Interruption(object sender, State target)
        {
            if (!currentState.IsInterruptable)
            {
                return;
            }

            stateStack.Push(currentState.AfterInterruptionState);

            ActionFactory.CreateRedirectionAction(target).Do();
        }
    }
}
