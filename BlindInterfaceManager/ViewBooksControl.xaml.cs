﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace BlindInterfaceManager
{
    /// <summary>
    /// Interaction logic for ViewBooksControl.xaml
    /// </summary>
    public partial class ViewBooksControl : UserControl
    {
        ObservableCollection<Book> books;

        public ViewBooksControl()
        {
            InitializeComponent();
            this.SetBooks();
        }

        public void Save()
        {
            for (int i = 0; i < books.Count; i++)
            {
                books[i].UpdateInFile(System.AppDomain.CurrentDomain.BaseDirectory + "books.txt");
            }
        }

        public void SetBooks()
        {
            books = Book.GetBooks(System.AppDomain.CurrentDomain.BaseDirectory + "books.txt");
            listBox.ItemsSource = books;
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            if (listBox.SelectedIndex != -1)
            {
                int index = listBox.SelectedIndex;
                if (MessageBox.Show("Дійсно видалити книжку " + books[index].BookName + "?", "Запитання", MessageBoxButton.YesNo, MessageBoxImage.Warning) == MessageBoxResult.Yes)
                {
                    books[index].RemoveFromFile(System.AppDomain.CurrentDomain.BaseDirectory + "books.txt");
                    books.RemoveAt(listBox.SelectedIndex);
                    listBox.SelectedItem = Math.Min(index, listBox.Items.Count);
                }
            }
        }

        private void listBox_SelectionChanged_1(object sender, SelectionChangedEventArgs e)
        {
            if (e.RemovedItems.Count != 0)
            {
                Book b = e.RemovedItems[0] as Book;
                b.UpdateInFile(System.AppDomain.CurrentDomain.BaseDirectory + "books.txt");
            }
        }

    }
}
