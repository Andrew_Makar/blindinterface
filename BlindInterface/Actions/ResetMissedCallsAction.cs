﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BlindInterface.Actions
{
    class ResetMissedCallsAction : Action
    {
        public ResetMissedCallsAction()
        {
            this.name = "reset missde calls action";
        }

        public override void Do()
        {
            base.Do();
            Skype.ResetMissedCalls();
            base.OnActionEnded();
        }
        public override void ForceStop()
        {
            base.ForceStop();
            if (this.ParentAction != null)
            {
                this.ParentAction.ForceStop();
            }
        }
    }
}
