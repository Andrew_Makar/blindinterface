﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using BlindInterface.States;

namespace BlindInterface.Actions
{
    class EventRedirectionAction : Action
    {
        object obj;
        string eventName;
        State target;
        EventHandler handler;
        Type type;

        public EventRedirectionAction(object obj, string eventName, State target)
        {
            this.obj = obj;
            this.eventName = eventName;
            this.target = target;
            if (obj is Type)
            {
                type = (Type)obj;
            }
            else
            {
                type = obj.GetType();
            }

            handler = new EventHandler((sender, e) => EventHandler());
        }
        public override void Do()
        {
            base.Do();

            Action.currentActions.Add(this);

            Attach();
        }
        public override void ForceStop()
        {
            base.ForceStop(); 
            Deattach();

            if (this.ParentAction != null)
            {
                this.ParentAction.ForceStop();
            }
        }

        private void Attach()
        {
            EventInfo eventInfo = type.GetEvent(eventName);
            eventInfo.AddEventHandler(obj, handler);
        }
        private void Deattach()
        {
            EventInfo eventInfo = type.GetEvent(eventName);
            eventInfo.RemoveEventHandler(obj, handler);
        }

        private void EventHandler()
        {
            Deattach();

            Action redirection = new RedirectionAction(target);
            redirection.ActionEnded += redirection_ActionEnded;
            redirection.Do();
        }

        void redirection_ActionEnded()
        {
            base.OnActionEnded();
        }

    }
}
