﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace BlindInterfaceManager
{
    class Book
    {
        string filePath;
        string titlePath;
        string bookName;
        double percentage;

        public string FilePath
        {
            get
            {
                return filePath;
            }
            set
            {
                filePath = value;
            }
        }
        public string TitlePAth
        {
            get
            {
                return titlePath;
            }
            set
            {
                titlePath = value;
            }
        }
        public string BookName
        {
            get
            {
                return bookName;
            }
            set
            {
                bookName = value;
            }
        }
        public double Progress
        {
            get
            {
                return percentage;
            }
            set
            {
                percentage = value;
            }
        }

        public Book(string filePath, string titlePath, string bookName)
            : this(filePath, titlePath, bookName, 0) { }
        private Book(string filePath, string titlePath, string bookName, double percentage)
        {
            this.filePath = filePath;
            this.titlePath = titlePath;
            this.bookName = bookName;
            this.percentage = percentage;
        }

        public void AddToFile(string path)
        {
            string file = System.IO.File.ReadAllText(path);
            file += filePath + "\r\n";
            file += titlePath + "\r\n";
            file += bookName + "\r\n";
            file += percentage + "\r\n";
            file += "-------------------------\r\n";
            System.IO.File.WriteAllText(path, file);
        }
        public void UpdateInFile(string path)
        {
            List<string> res = new List<string>();
            string[] file = System.IO.File.ReadAllLines(path);
            for (int i = 0; i < file.Length; i += 5)
            {
                string filePath = file[i];
                string titlePath = file[i + 1];
                string bookName = file[i + 2];

                char separator = Thread.CurrentThread.CurrentCulture.NumberFormat.NumberDecimalSeparator[0];
                file[i + 3] = file[i + 3].Replace('.', separator);
                file[i + 3] = file[i + 3].Replace(',', separator);

                double percentage = double.Parse(file[i + 3]);
                if (bookName != this.bookName)
                {
                    res.Add(filePath);
                    res.Add(titlePath);
                    res.Add(bookName);
                    res.Add(percentage.ToString());
                    res.Add("-------------------------");
                }
                else
                {
                    res.Add(filePath);
                    res.Add(titlePath);
                    res.Add(bookName);
                    res.Add(this.percentage.ToString());
                    res.Add("-------------------------");
                }
            }

            file = new string[res.Count];
            for (int i = 0; i < res.Count; i++)
            {
                file[i] = res[i];
            }
            System.IO.File.WriteAllLines(path, file);
        }

        public static ObservableCollection<Book> GetBooks(string path)
        {
            ObservableCollection<Book> res = new ObservableCollection<Book>();
            string[] file = System.IO.File.ReadAllLines(path);
            for (int i = 0; i < file.Length; i += 5)
            {
                string filePath = file[i];
                string titlePath = file[i + 1];
                string bookName = file[i + 2];


                char separator = Thread.CurrentThread.CurrentCulture.NumberFormat.NumberDecimalSeparator[0];
                file[i + 3] = file[i + 3].Replace('.', separator);
                file[i + 3] = file[i + 3].Replace(',', separator);

                double percentage = double.Parse(file[i + 3]);
                res.Add(new Book(filePath, titlePath, bookName, percentage));
            }
            return res;
        }
        public static bool ContainsBook(string path, string name)
        {
            string[] file = System.IO.File.ReadAllLines(path);
            for (int i = 0; i < file.Length; i += 5)
            {
                string bookName = file[i + 2];
                if (bookName == name)
                {
                    return true;
                }
            }
            return false;
        }
        public void RemoveFromFile(string path)
        {
            List<string> res = new List<string>();
            string[] file = System.IO.File.ReadAllLines(path);
            for (int i = 0; i < file.Length; i += 5)
            {
                string filePath = file[i];
                string titlePath = file[i + 1];
                string bookName = file[i + 2];

                char separator = Thread.CurrentThread.CurrentCulture.NumberFormat.NumberDecimalSeparator[0];
                file[i + 3] = file[i + 3].Replace('.', separator);
                file[i + 3] = file[i + 3].Replace(',', separator);

                double percentage = double.Parse(file[i + 3]);
                if (bookName != this.bookName)
                {
                    res.Add(filePath);
                    res.Add(titlePath);
                    res.Add(bookName);
                    res.Add(percentage.ToString());
                    res.Add("-------------------------");
                }
            }

            file = new string[res.Count];
            for (int i = 0; i < res.Count; i++)
            {
                file[i] = res[i];
            }
            System.IO.File.WriteAllLines(path, file);

            try
            {
                System.IO.File.Delete(this.filePath);
            }
            catch (Exception)
            {   
            }
            try
            {
                System.IO.File.Delete(this.titlePath);
            }
            catch (Exception)
            {
            }

        }
    }
}
