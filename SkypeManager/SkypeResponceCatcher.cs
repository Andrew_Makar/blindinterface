﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using SkypeManager.SkypeResponces;
using SkypeControl;

namespace SkypeManager
{
    class SkypeResponceCatcher
    {

        static Dictionary<string, SkypeResponce> responces = new Dictionary<string, SkypeResponce>();
        public static Dictionary<string, SkypeResponce> Responces
        {
            get
            {
                return responces;
            }
        }

        public static void Responce(object sender, SkypeResponseEventArgs theEventArgs)
        {
            
            string responceString = theEventArgs.Response;
            MessageBox.Show(responceString);
            SkypeResponce responce = SkypeResponceParser.ParseResponce(responceString);
            if (responce is CommandResponce)
            {
                CommandResponce cr = responce as CommandResponce;
                responces.Add(cr.CommandId, cr.Responce);
            }
        }
    }
}
