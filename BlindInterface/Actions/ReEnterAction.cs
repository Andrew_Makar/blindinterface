﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Threading;

namespace BlindInterface.Actions
{
    class ReEnterAction : Action
    {
        DispatcherTimer timer;

        public ReEnterAction()
        {
            this.name = "Reenter action";
        }

        public override void Do()
        {
            base.Do();

            Action.currentActions.Add(this);

            timer = new DispatcherTimer();
            timer.Interval = new TimeSpan(0, 0, 30);
            timer.Tick += timer_Tick;
            timer.Start();
        }

        void timer_Tick(object sender, EventArgs e)
        {
            timer.Stop();
            base.OnActionEnded();
            StateManager.CurrentState.Enter();
        }
        public override void ForceStop()
        {
            base.ForceStop();

            if (Action.currentActions.Contains(this))
            {
                Action.currentActions.Remove(this);
            }
            timer.Stop();
        }
    }
}
