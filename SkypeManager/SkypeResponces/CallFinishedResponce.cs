﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SkypeManager.SkypeResponces
{
    class CallFinishedResponce : CallStatusResponce
    {
        public CallFinishedResponce(int id)
        {
            this.id = id;
            this.status = "call finished";
            this.callStatus = SkypeManager.CallStatus.Finished;
        }

        public static bool TryParse(string responce, out SkypeResponce result)
        {
            return CallStatusResponce.TryParse(responce, "FINISHED", out result, typeof(CallFinishedResponce));
        }
    }
}
