﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BlindInterface
{
    class TextMessages
    {
        public static string StartEntry
        {
            get
            {
                return "Вас приветствует программа Невидимка";
            }
        }
        public static string MainEntry
        {
            get
            {
                return "Вы находитесь в главном меню. \r\nЧтобы читать книги нажмите левую кнопку. \r\nЧтобы позвонить по скайпу нажмите правую кнопку";
            }
        }
        public static string BooksEntry
        {
            get
            {
                return "Ви вибрали книжки.";
            }
        }
        public static string ChooseBookEntry1
        {
            get
            {
                return "Книга ";
            }
        }
        public static string ChooseBookEntry2
        {
            get
            {
                return ". \r\nНажмите левую кнопку для её чтения, или правую для выбора другой.";
            }
        }
        public static string ReadBookEntry1
        {
            get
            {
                return "Вы выбрали книгу ";
            }
        }
        public static string ReadBookEntry2
        {
            get
            {
                return ". \r\nДля паузы нажмите левую кнопку.";
            }
        }
        public static string ActualReadBookEntry
        {
            get
            {
                return "Текст книги ";
            }
        }
        public static string PauseEntry
        {
            get
            {
                return "Пауза. \r\nНажмите левую кнопку для продолжения или правую для выхода в главное меню.";
            }
        }
        public static string EndOfBookEntry1
        {
            get
            {
                return "Конец книги ";
            }
        }
        public static string EndOfBookEntry2
        {
            get
            {
                return "\r\nДля выхода в главное меню нажмите левую кнопку.";
            }
        }
        public static string NoBooksEntry
        {
            get
            {
                return "Книги не загружены. \r\nДля перехода в главное меню нажмите левую кнопку.";
            }
        }
        public static string MissedCallsEntry
        {
            get
            {
                return "У вас есть пропущенный звонок";
            }
        }
        public static string AvailableContactsNoContactsEntry
        {
            get
            {
                return "Вы никому не можете позвонить";
            }
        }
        public static string AvailableContactsEntry
        {
            get
            {
                return "Для звонка нажмите левую, иначе правую";
            }
        }
        public static string ChooseContactEntry
        {
            get
            {
                return "Позвонить ";
            }
        }
        public static string CallEntry1
        {
            get
            {
                return "Набираем ";
            }
        }
        public static string CallEntry2
        {
            get
            {
                return ". Для окончания разговора нажмите левую";
            }
        }
        public static string CallFinishedEntry
        {
            get
            {
                return "Разговор окончен";
            }
        }
        public static string CallDeniedEntry
        {
            get
            {
                return "Абонент не отвечает";
            }
        }
        public static string IncomingCallEntry
        {
            get
            {
                return "Для начала разговора нажмите левую. Иначе правую";
            }
        }
        public static string EndIncomingCallEntry
        {
            get
            {
                return "Разговор окончен";
            }
        }
        public static string DenyIncomingCallEntry
        {
            get
            {
                return "Вы отказались от разговора";
            }
        }
        public static string ContactFinishedIncomingCallEntry
        {
            get
            {
                return "Абонент отказался от разговора";
            }
        }
        public static string ActualIncomingCallEntry
        {
            get
            {
                return "Разговор";
            }
        }
    }
}
