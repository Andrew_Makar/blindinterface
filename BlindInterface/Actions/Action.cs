﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BlindInterface.Actions
{
    public abstract class Action
    {
        virtual public void Do()
        {
            Logger.Log(String.Format("Action \"{0}\" do", this.name));
          //  Action.currentActions.Add(this);
        }
        virtual public void ForceStop()
        {
            Logger.Log(String.Format("Action \"{0}\" force stop", this.name));
            /*while (Action.currentActions.Contains(this))
            {
                Action.currentActions.Remove(this);
            }

            if (!isOn)
            {
                return;
            }

            isOn = false;
            if (this.parentAction != null)
            {
                this.parentAction.ForceStop();
            }*/
        }

        protected bool isOn;
        protected string name;
        public string Name
        {
            get
            {
                return name;
            }
        }

        protected Action()
        {
            this.name = "Anonymous action";
        }

        Action parentAction;
        public Action ParentAction
        {
              get
            {
                return parentAction;
            }
            set
            {
                parentAction = value;
            }
        }

        public delegate void ActionEndedEventHandler();
        public event ActionEndedEventHandler ActionEnded;

        protected void OnActionEnded()
        {
            Logger.Log(String.Format("Action \"{0}\" ended", this.name));

            while (Action.currentActions.Contains(this))
            {
                Action.currentActions.Remove(this);
            }

            if (ActionEnded != null)
            {
                ActionEnded();
            }
        }

        static public List<Action> currentActions = new List<Action>();
    }
}
