﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BlindInterface.Actions
{
    class UpdateContactsOnlineAction : Action
    {
        public UpdateContactsOnlineAction()
        {
            this.name = "Update contacts online action";
        }

        public override void Do()
        {
            Skype.UpdateContactsOnline();
            base.OnActionEnded();
        }
        public override void ForceStop()
        {
            base.ForceStop();

            if (this.ParentAction != null)
            {
                this.ParentAction.ForceStop();
            }
        }
    }
}
