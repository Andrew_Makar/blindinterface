﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;

namespace BlindInterface.Actions
{
    class AudioAction : Action
    {
        Uri uri;
        protected double progress;

        public AudioAction(Uri uri, double progress)
        {
            this.uri = uri;
            this.progress = progress;

            this.name = "Audio action";
        }

        public override void Do()
        {
            base.Do();

            Action.currentActions.Add(this);

            isOn = true;
            Debug.WriteLine("Audio action started");
            AudioManager.Play(uri, this, progress);
            AudioManager.MediaEndedEvent += AudioManager_MediaEndedEvent;
        }

        protected virtual void AudioManager_MediaEndedEvent()
        {

            isOn = false;
            Debug.WriteLine("Audio action ended");
            AudioManager.MediaEndedEvent -= AudioManager_MediaEndedEvent;
            base.OnActionEnded();
        }

        public override void ForceStop()
        {
            if (Action.currentActions.Contains(this))
            {
                Action.currentActions.Remove(this);
            }

            if (!isOn)
            {
                return;
            }

            base.ForceStop();

            isOn = false;
            Debug.WriteLine("Audio action force stopped");
            AudioManager.MediaEndedEvent -= AudioManager_MediaEndedEvent;
            AudioManager.Stop();

            if (this.ParentAction != null)
            {
                this.ParentAction.ForceStop();
            }
        }
    }
}
