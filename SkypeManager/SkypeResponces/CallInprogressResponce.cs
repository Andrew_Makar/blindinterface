﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SkypeManager.SkypeResponces
{
    class CallInprogressResponce : CallStatusResponce
    {
        public CallInprogressResponce(int id)
        {
            this.id = id;
            this.status = "call is in progress";
            this.callStatus = SkypeManager.CallStatus.InProgress;
        }

        public static bool TryParse(string responce, out SkypeResponce result)
        {
            return CallStatusResponce.TryParse(responce, "INPROGRESS", out result, typeof(CallInprogressResponce));
        }
    }
}
