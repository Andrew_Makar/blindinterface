﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;

using SkypeControl;
using MyTestApp.SkypeResponces;
using MyTestApp.SkypeManagerStates;
using System.Windows.Threading;

namespace MyTestApp
{
    class SkypeManager
    {

        SkypeProxy skype;
        Dictionary<string, SkypeResponce> responces;
        ISkypeManagerState state;
        Dispatcher dispatcher;

        public SkypeManager(Dispatcher dispatcher)
        {
            this.dispatcher = dispatcher;

            responces = new Dictionary<string, SkypeResponce>();

            
            Task.Factory.StartNew(() =>
            {
                skype = new SkypeProxy();
                skype.SkypeResponse += skype_SkypeResponse;
                skype.Conect();
            });
        }

        void skype_SkypeResponse(object theSender, SkypeResponseEventArgs theEventArgs)
        {
            string responceString = theEventArgs.Response;
            SkypeResponce responce = SkypeResponceParser.ParseResponce(responceString);
            if (responce is CommandResponce)
            {
                CommandResponce cr = responce as CommandResponce;
                responces.Add(cr.CommandId, cr.Responce);
            }
        }

        public SkypeResponce QueryCommand(string command)
        {
            string commandId = "#" + Helper.GetRandomString(7);
           // skype.Invoker.Invoke(new Action(() => skype.Command(commandId + " " + command)));
            skype.Command(commandId + " " + command);

            //typeof(SkypeProxy).GetMethod("Command").Invoke(skype, new object[] { commandId + " " + command });

            while (!responces.ContainsKey(commandId))
            {
                Thread.Sleep(100);
            }
            
            SkypeResponce res = responces[commandId];
            responces.Remove(commandId);
            return res;
        }


        public void Call(string skypeId)
        {
            string command = "CALL " + skypeId;
            CallStatusResponce responce = this.QueryCommand(command) as CallStatusResponce;
            int callId = responce.Id;

            state = new SkypeManagerCallingState(callId, this);
            state.Enter();

        }
        public void FinishCall()
        {
            if (state is SkypeManagerCallingState)
            {
                (state as SkypeManagerCallingState).FinishCall();
            }
        }
        public void AnswerCall() { }
        public void DenyCall() { }

        public delegate void CallRefusedEventHandler();
        public event CallRefusedEventHandler CallRefused;
        public void OnCallRefused()
        {
            state = null;
            if (CallRefused != null)
            {
                CallRefused();
            }
        }

        public delegate void CallFinishedEventHandler();
        public event CallFinishedEventHandler CallFinished;
        public void OnCallFinished()
        {
            state = null;
            if (CallFinished != null)
            {
                CallFinished();
            }
        }

        public delegate void IncomingCallEventHandler(string partnerId);
        public event IncomingCallEventHandler IncomingCall;
        public void OnIncomingCall(string partnerId)
        {
            if (IncomingCall != null)
            {
                IncomingCall(partnerId);
            }
        }

    }
}
