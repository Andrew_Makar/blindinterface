﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BlindInterface.Actions
{
    class FinishCallAction:Action
    {
        public FinishCallAction()
        {
            this.name = "Finish call action";
        }

        public override void Do()
        {
            base.Do();
            Skype.FinishCall();
            base.OnActionEnded();
        }
        public override void ForceStop()
        {
            base.ForceStop();

            if (this.ParentAction != null)
            {
                ParentAction.ForceStop();
            }
        }
    }
}
