﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using BlindInterface.States;
using System.Windows.Threading;
using BlindInterface.Interrupters;
using System.Threading;
using System.Runtime.InteropServices;

namespace BlindInterface
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {

          //  Thread.Sleep(10000);

            Logger.Log("Application started");

            InitializeComponent();
            Logger.Log("Components initialized");

            AudioManager.Voulme = 1;

            TextManager.TextBlock = textBlock;

            List<Interrupter> interrupters = InterrupterFactory.CreateCallInterrupters();
            foreach (var interrupter in interrupters)
            {
                StateManager.AddInterrupter(interrupter);
            }

            StateManager.Redirect(StateFactory.CreateStartState());

            DispatcherTimer t = new DispatcherTimer();
            t.Interval = new TimeSpan(0, 0, 5);
            t.Tick += t_Tick;
            t.Start();
            
        }

        void t_Tick(object sender, EventArgs e)
        {
            this.Topmost = true;
            this.Activate();
            this.Topmost = false;
        }

        private void Grid_MouseDown_1(object sender, MouseButtonEventArgs e)
        {

            Logger.Log("Mouse down");
            if (e.LeftButton == MouseButtonState.Pressed)
            {
                StateManager.CurrentState.Action1();
            }
            else if (e.RightButton == MouseButtonState.Pressed)
            {
                StateManager.CurrentState.Action2();
            }
        }

        private void Grid_MouseWheel_1(object sender, MouseWheelEventArgs e)
        {
            Logger.Log(String.Format("Mouse wheel {0}",e.Delta));
            AudioManager.Voulme += e.Delta / 3000.0;
        }

        private void Grid_KeyUp_1(object sender, KeyEventArgs e)
        {
            Logger.Log(String.Format("Key {0} pressed", e.Key));

            if (e.Key == Key.Escape)
            {
                Process.Start(System.AppDomain.CurrentDomain.BaseDirectory + "manager.exe");
                this.Close();
            }
        }

        private void Window_Closing_1(object sender, System.ComponentModel.CancelEventArgs e)
        {
            Logger.Log("Application closed");
        }

        [DllImport("User32.dll")]
        private static extern bool SetCursorPos(int X, int Y);

        private void Window_MouseMove_1(object sender, MouseEventArgs e)
        {
            SetCursorPos(100, 100);
        }
    }
}
