﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SkypeManager.SkypeResponces
{
    class CallRingingResponce : CallStatusResponce
    {
        public CallRingingResponce(int id)
        {
            this.id = id;
            this.status = "calling";
            this.callStatus = SkypeManager.CallStatus.Ringing;
        }

        public static bool TryParse(string responce, out SkypeResponce result)
        {
            return CallStatusResponce.TryParse(responce, "RINGING", out result, typeof(CallRingingResponce));
        }

        
    }
}
