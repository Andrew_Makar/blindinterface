﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BlindInterface.Actions
{
    class CallAction : SkypeCallAction
    {
        string skypeId;

        public CallAction(string skypeId)
        {
            this.skypeId = skypeId;
            this.name = "call action";
        }

        public override void Do()
        {
            base.Do();

            Skype.Call(skypeId);
        }
    }
}
