﻿using BlindInterface.States;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BlindInterface.Interrupters
{
    abstract class Interrupter
    {
        public event EventHandler<State> Interruption;
        protected void OnInterruption()
        {
            if (Interruption != null)
            {
                Interruption(this, startState);
            }
        }

        protected State startState;
        public State StartState
        {
            get
            {
                return startState;
            }
        }

        public abstract void DenyInterruption();
    }
}
