﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using BlindInterface.Actions;
using BlindInterface.States;
using Action = BlindInterface.Actions.Action;

namespace BlindInterface.Controllers
{
    class ControllerFactory
    {
        public static Controller CreateRedirectionController(State state1, State state2)
        {
            Logger.Log(String.Format("Redirection controller to \"{0}\", \"{1}\" created", state1 != null ? state1.Name : "null", state2 != null ? state2.Name : "null"));

            Action action1;
            if (state1 != null)
            {
                action1 = ActionFactory.CreateRedirectionAction(state1);
            }
            else
            {
                action1 = ActionFactory.CreateEmptyAction();
            }

            Action action2;
            if (state2 != null)
            {
                action2 = ActionFactory.CreateRedirectionAction(state2);
            }
            else
            {
                action2 = ActionFactory.CreateEmptyAction();
            }
            return new Controller(action1, action2);
        }
        public static Controller CreateActionController(Action action1, Action action2)
        {
            if (action1 == null)
            {
                action1 = ActionFactory.CreateEmptyAction();
            }
            if (action2 == null)
            {
                action2 = ActionFactory.CreateEmptyAction();
            }

            return new Controller(action1, action2);
        }
    }
}
