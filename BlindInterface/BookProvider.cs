﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace BlindInterface
{
    class BookProvider
    {
        static List<Book> books;

        public static int Count
        {
            get
            {
                return books.Count;
            }
        }

        static BookProvider()
        {
            Logger.Log("Book provider started initializing");
            books = new List<Book>();

            string[] file = System.IO.File.ReadAllLines(System.AppDomain.CurrentDomain.BaseDirectory + "books.txt");
            for (int i = 0; i < file.Length; i += 5)
            {
                string filePath = file[i];
                string titlePath = file[i + 1];
                string bookName = file[i + 2];
                string percentage = file[i + 3];
                books.Add(new Book(filePath, titlePath, bookName, percentage));
            }

            Logger.Log("Book provider initialized");
        }

        public static Uri GetBookUri(int index)
        {
            //throw new NotImplementedException();
            return books[index].Uri;
        }
        public static Uri GetBookTitleUri(int index)
        {
            //throw new NotImplementedException();
            return books[index].TitleUri;
        }
        public static string GetBookName(int index)
        {
            return books[index].Name;
        }
        public static double GetBookProgress(int index)
        {
            return books[index].Progress;
        }
        public static void SetBookProgress(int index, double progress)
        {
            books[index].Progress = progress;
            books[index].UpdateInFile(System.AppDomain.CurrentDomain.BaseDirectory + "books.txt");
        }
    }
}
