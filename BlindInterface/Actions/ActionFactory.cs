﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using BlindInterface.States;

namespace BlindInterface.Actions
{
    class ActionFactory
    {
        private static void LogActionCreation(Action action)
        {
            Logger.Log(String.Format("Action \"{0}\" created", action.Name));
        }

        public static Action CreateAudioAction(Uri uri)
        {
            Action res = new AudioAction(uri, 0);
            LogActionCreation(res);
            return res;
        }
        public static Action CreateAudioAction(Uri uri, double progress)
        {
            Action res = new AudioAction(uri, progress);
            LogActionCreation(res);
            return res;
        }
        public static Action CreateAudioAction(List<Uri> uris)
        {
            List<Action> actions = new List<Action>();
            for (int i = 0; i < uris.Count; i++)
            {
                actions.Add(ActionFactory.CreateAudioAction(uris[i]));
            }
            Action res = new MultiAction(actions);
            LogActionCreation(res);
            return res;
        }
        public static Action CreateAudioAction(Uri uri1, Uri uri2)
        {
            List<Uri> uris = new List<Uri>();
            uris.Add(uri1);
            uris.Add(uri2);
            return ActionFactory.CreateAudioAction(uris);
        }
        public static Action CreateAudioAction(Uri uri1, Uri uri2, Uri uri3)
        {
            List<Action> actions = new List<Action>();
            actions.Add(ActionFactory.CreateAudioAction(uri1));
            actions.Add(ActionFactory.CreateAudioAction(uri2));
            actions.Add(ActionFactory.CreateAudioAction(uri3));
            Action res = new MultiAction(actions);
            LogActionCreation(res);
            return res;
        }
        public static Action CreateRedirectionAction(State target)
        {
            Action res = new RedirectionAction(target);
            LogActionCreation(res);
            return res;
        }
        public static Action CreateEmptyAction()
        {
            Action res = new EmptyAction();
            LogActionCreation(res);
            return res;
        }
        public static Action CreateMultiAction(List<Action> actions)
        {
            Action res = new MultiAction(actions);
            LogActionCreation(res);
            return res;
        }
        public static Action CreateMultiAction(Action action1, Action action2)
        {
            List<Action> actions = new List<Action>();
            actions.Add(action1);
            actions.Add(action2);
            Action res = ActionFactory.CreateMultiAction(actions);
            LogActionCreation(res);
            return res;
        }
        public static Action CreateMultiAction(Action action1, Action action2, Action action3)
        {
            List<Action> actions = new List<Action>();
            actions.Add(action1);
            actions.Add(action2);
            actions.Add(action3);
            Action res = ActionFactory.CreateMultiAction(actions);
            return res;
        }
        public static Action CreateTextAction(string message)
        {
            Action res = new TextAction(message);
            LogActionCreation(res);
            return res;
        }
        public static Action CreateAudioBookAction(Uri uri, double progress, int index)
        {
            Action res = new AudioBookAction(uri, progress, index);
            LogActionCreation(res);
            return res;
        }
        public static Action CreateReEnterAction()
        {
            Action res = new ReEnterAction();
            LogActionCreation(res);
            return res;
        }
        public static Action CreateConditionAction(Func<bool> condition, Action ifTrue, Action ifFalse)
        {
            if (ifTrue == null)
            {
                ifTrue = ActionFactory.CreateEmptyAction();
            }
            if (ifFalse == null)
            {
                ifFalse = ActionFactory.CreateEmptyAction();
            }
            return new ConditionAction(condition, ifTrue, ifFalse);
        }
        public static Action CreateConditionAction(Func<bool> condition, Action ifTrue)
        {
            return CreateConditionAction(condition, ifTrue, null);
        }
        public static Action CreateCallAction(string skypeId)
        {
            return new CallAction(skypeId);
        }
        public static Action CreateFinishCallAction()
        {
            return new FinishCallAction();
        }
        public static Action CreateEventRedirectionAction(object obj, string eventName, State target)
        {
            return new EventRedirectionAction(obj, eventName, target);
        }
        public static Action CreateSimultaneousAction(List<Action> actions)
        {
            return new SimultaneousAction(actions);
        }
        public static Action CreateSimultaneousAction(Action action1, Action action2)
        {
            List<Action> actions = new List<Action> { action1, action2 };
            return ActionFactory.CreateSimultaneousAction(actions);
        }
        public static Action CreateSimultaneousAction(Action action1, Action action2, Action action3)
        {
            List<Action> actions = new List<Action> { action1, action2, action3 };
            return ActionFactory.CreateSimultaneousAction(actions);
        }
        public static Action CreateFinishSubroutineAction()
        {
            return new FinishSubroutineAction();
        }
        public static Action CreateAnswerCallAction()
        {
            return new AnswerCallAction();
        }
        public static Action CreateUpdateContactsOnlineAction()
        {
            return new UpdateContactsOnlineAction();
        }
        public static Action CreateDenyCallAction()
        {
            return new DenyCallAction();
        }
        public static Action CreateResetMissedCallsAction()
        {
            return new ResetMissedCallsAction();
        }
    }
}
