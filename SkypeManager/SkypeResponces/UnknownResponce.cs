﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SkypeManager.SkypeResponces
{
    class UnknownResponce : SkypeResponce
    {
        public static bool TryParse(string responce, out SkypeResponce result)
        {
            result = new UnknownResponce();
            return true;
        }
    }
}
