﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Threading;
using SkypeManager.SkypeResponces;

namespace SkypeManager.SkypeManagerStates
{
    class SkypeManagerWaitingState : ISkypeManagerState
    {
        Manager manager;
        int incomingCallId;
        int lastIncomingCallId;
        bool callInProgress;
        DispatcherTimer timer;

        public SkypeManagerWaitingState(Manager manager)
        {
            this.manager = manager;
            callInProgress = false;

            timer = new DispatcherTimer();
            timer.Interval = new TimeSpan(0, 0, 1);
            timer.Tick += timer_Tick;
        }

        void timer_Tick(object sender, EventArgs e)
        {
            CheckIncomingCall();
        }

        public void Enter()
        {
            incomingCallId = -1;
            //CheckIncomingCall();
            timer.Start();
        }
        public void Leave()
        {
            callInProgress = true;
        }
        public void Call(string skypeId)
        {
            string command = "CALL " + skypeId;
            callInProgress = true;
            manager.QueryCommand(command, (responce) =>
            {
                int callId = (responce as CallStatusResponce).Id;

                manager.State = new SkypeManagerCallingState(callId, manager);
                manager.State.Enter();
            });
        }
        public void FinishCall()
        {
            throw new Exception("Can't finish call when call is not on");
        }
        public void AnswerCall()
        {
            if (incomingCallId == -1)
            {
                throw new Exception("Cant answer call when there is no incoming call");
            }
            string command = "SET CALL " + incomingCallId + " STATUS INPROGRESS";
            manager.QueryCommand(command, (responce) =>
            {
                callInProgress = true;
                manager.State = new SkypeManagerCallingState(incomingCallId, manager);
                manager.State.Enter();
            });
        }
        public void DenyCall()
        {
            //throw new NotImplementedException();
            if (incomingCallId == -1)
            {
                throw new Exception("Cant deny call when there is no incoming calls");
            }
            string command = "SET CALL " + incomingCallId + " STATUS FINISHED";
            manager.QueryCommand(command, (responce) => { });

            incomingCallId = -1;
            manager.OnCallFinished();
        }

        private void CheckIncomingCall()
        {
            timer.Stop();
            if (callInProgress)
            {
                return;
            }
            string command = "SEARCH ACTIVECALLS";
            manager.QueryCommand(command, (responce) =>
            {
                List<int> callsId = (responce as SearchCallsResponce).CallsIds;
                if (callsId.Count != 0)
                {
                    if (!callsId.Contains(lastIncomingCallId))
                    {
                        incomingCallId = callsId[0];
                        lastIncomingCallId = incomingCallId;
                        string command2 = "GET CALL " + incomingCallId.ToString() + " PARTNER_HANDLE";
                        manager.QueryCommand(command2, (responce2) =>
                        {
                            manager.OnIncomingCall((responce2 as CallPartnerHandleResponce).PartnerHandle);

                            timer.Start();
                        });
                    }
                    else
                    {
                        timer.Start();
                    }
                }
                else
                {
                    if (incomingCallId != -1)
                    {
                        incomingCallId = -1;
                        manager.OnCallFinished();
                    }

                    timer.Start();
                }
            });


        }
    }
}
