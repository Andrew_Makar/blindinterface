﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SkypeManager.SkypeResponces
{
    class CommandResponce : SkypeResponce
    {
        private SkypeResponce responce;
        private string commandId;
        public SkypeResponce Responce
        {
            get
            {
                return responce;
            }
        }
        public string CommandId
        {
            get
            {
                return commandId;
            }
        }


        public CommandResponce(SkypeResponce responce, string commandId)
        {
            this.responce = responce;
            this.commandId = commandId;
        }

        public static bool TryParse(string responce, out SkypeResponce result)
        {
            result = null;

            string[] tokens = responce.Split(' ');
            if (tokens.Length == 0)
            {
                return false;
            }
            if (tokens[0][0] != '#')
            {
                return false;
            }
            string commandId = tokens[0];


            responce = String.Join(" ", tokens, 1, tokens.Length - 1);

            SkypeResponce responceInstance = SkypeResponceParser.ParseResponce(responce);

            result =  new CommandResponce(responceInstance, commandId);
            return true;

        }
    }
}
