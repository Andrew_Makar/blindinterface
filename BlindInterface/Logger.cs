﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace BlindInterface
{
    class Logger
    {
        public const bool IsEnabled = true;
        private static string path;

        static Logger()
        {
            string t = Logger.GetTime();
            t += ".txt";
            path = t.Replace(':','-');
            path = System.AppDomain.CurrentDomain.BaseDirectory + path;
            System.IO.File.CreateText(path).Close();

            Logger.Log("Logger initialized");
        }

        public static void Log(string message)
        {
            if (IsEnabled)
            {
                string text = String.Format("{0}: {1}", Logger.GetTime(), message);
                try
                {

                    using (StreamWriter writer = System.IO.File.AppendText(Logger.path))
                    {
                        writer.WriteLine(text);
                    }
                }
                catch (Exception e)
                {
                    throw;
                }
            }
        }

        private static string GetTime()
        {
            return String.Format("{0:00}:{1:00}:{2:00}.{3:000}", DateTime.Now.Hour, DateTime.Now.Minute, DateTime.Now.Second, DateTime.Now.Millisecond);
        }
    }
}
