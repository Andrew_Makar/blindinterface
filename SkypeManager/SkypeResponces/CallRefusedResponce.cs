﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SkypeManager.SkypeResponces
{
    class CallRefusedResponce : CallStatusResponce
    {
        public CallRefusedResponce(int id)
        {
            this.id = id;
            this.status = "call refused";
            this.callStatus = SkypeManager.CallStatus.Refused;
        }

        public static bool TryParse(string responce, out SkypeResponce result)
        {
            return CallStatusResponce.TryParse(responce, "REFUSED", out result, typeof(CallRefusedResponce));
        }
    }
}
