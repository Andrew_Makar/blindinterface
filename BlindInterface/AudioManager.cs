﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Windows.Forms;
using System.Windows.Media;
using Action = BlindInterface.Actions.Action;

namespace BlindInterface
{
    class AudioManager
    {
        static MediaPlayer player;

        static AudioManager()
        {
            player = new MediaPlayer();
            player.MediaEnded += player_MediaEnded;
            player.Volume = 1;
        }

        private static double volume;

        public static double Voulme
        {
            get
            {
                return volume;
            }
            set
            {
                value = Math.Max(value, 0);
                value = Math.Min(value, 1);

                volume = value;
                player.Volume = value;
            }
        }

        private static Action playingAction;
        public static Action PlayingAction
        {
            get
            {
                return playingAction;
            }
        }

        public static void Play(Uri source, Action action, double percentage)
        {
            while (true)
            {
                player.Stop();
                player = new MediaPlayer();
                Thread.Sleep(100);
                player.MediaEnded += player_MediaEnded;
                player.Volume = AudioManager.volume;
                playingAction = action;
                player.Open(source);
                Logger.Log(String.Format("Audio manager started opening \"{0}\"", source));
                int cnt = 0;
                while (!player.NaturalDuration.HasTimeSpan)
                {
                    cnt++;
                    if (cnt > 50000000)
                    {
                        break;
                    }
                } //MessageBox.Show(cnt.ToString());
                if (cnt <= 50000000)
                {
                    break;
                }
                Logger.Log("Player can't open media. Resetting player");
              //  MessageBox.Show(cnt.ToString());
            }
            Logger.Log(String.Format("Audio manager opened media"));
            double length = player.NaturalDuration.TimeSpan.TotalSeconds;
            int position = (int)(length * percentage / 100);

            position -= 5;
            position = Math.Max(position, 0);

            player.Position = new TimeSpan(0, 0, position);
            player.Play();
            Logger.Log(String.Format("Audio manager started playing media"));
        }
        public static void Stop()
        {
            Logger.Log("Audio managed force stopped");
            playingAction = null;
            player.Stop();
        }

        public static double Progress
        {
            get
            {
                Thread.Sleep(100);
                double length = player.NaturalDuration.TimeSpan.TotalSeconds;
                double position = player.Position.TotalSeconds;
                return position / length * 100;
            }
        }

        public delegate void MediaEndedEventHandler();
        public static event MediaEndedEventHandler MediaEndedEvent;

        static void player_MediaEnded(object sender, EventArgs e)
        {
            Logger.Log("Audio manager media ended");
            playingAction = null;
            if (MediaEndedEvent != null)
            {
                MediaEndedEvent();
            }
        }
    }
}
