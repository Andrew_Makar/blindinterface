﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BlindInterface.Actions
{
    class SimultaneousAction : Action
    {
        List<Action> actions;
        public SimultaneousAction(List<Action> actions)
        {
            this.actions = actions;
            for (int i = 0; i < actions.Count; i++)
            {
                actions[i].ParentAction = this;
            }
            this.name = "Simultaneous action";
        }

        public override void Do()
        {
            base.Do();

            isOn = true;

            Action.currentActions.Add(this);

            foreach(var action in actions)
            {
                action.ActionEnded += SimultaneousAction_ActionEnded;
            }

            foreach (var action in actions)
            {
                action.Do();
            }
        }
        public override void ForceStop()
        {

            if (!isOn)
            {
                return;
            }
            isOn = false;

            base.ForceStop();

            foreach (var action in actions)
            {
                action.ActionEnded -= SimultaneousAction_ActionEnded;
                action.ForceStop();
            }

            if (this.ParentAction != null)
            {
                this.ParentAction.ForceStop();
            }
        }

        void SimultaneousAction_ActionEnded()
        {
            foreach (var action in actions)
            {
                action.ActionEnded -= SimultaneousAction_ActionEnded;
                action.ForceStop();
            }

            base.OnActionEnded();
        }
    }
}
