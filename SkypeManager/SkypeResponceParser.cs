﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using SkypeManager.SkypeResponces;

namespace SkypeManager
{
    class SkypeResponceParser
    {


        public static SkypeResponce ParseResponce(string responce)
        {
            var responces = new []{     typeof(CommandResponce),
                                        typeof(CallFinishedResponce), 
                                        typeof(CallInprogressResponce), 
                                        typeof(CallRefusedResponce),
                                        typeof(CallRingingResponce),
                                        typeof(CallMissedResponce),
                                        typeof(SearchCallsResponce),
                                        typeof(CallPartnerHandleResponce),
                                        typeof(UserOnlinestatusResponce),
                                        typeof(CallUnknownResponce),
                                        typeof(UnknownResponce)};

            SkypeResponce result = null;
            foreach (var r in responces)
            {
                object[] parameters = new object[] { responce, null };

                if ((bool)r.GetMethod("TryParse").Invoke(null, parameters))
                {
                    result = (SkypeResponce)parameters[1];
                    break;
                }
            }


            return result;
        }
    }
}
