﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Controls;

namespace BlindInterface
{
    class TextManager
    {
        public static TextBlock TextBlock
        {
            get;
            set;
        }
        public static void Message(string message)
        {
            Logger.Log("Text manager messaged");
            if (TextBlock != null)
            {
                TextBlock.Text = message;
            }
        }
    }
}
