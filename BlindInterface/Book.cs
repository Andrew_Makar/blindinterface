﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Windows.Forms;

namespace BlindInterface
{
    class Book
    {
        Uri uri;
        Uri titleUri;
        string name;
        double progress;

        public Uri Uri
        {
            get
            {
                return uri;
            }
        }
        public Uri TitleUri
        {
            get
            {
                return titleUri;
            }
        }
        public string Name
        {
            get
            {
                return name;
            }
        }
        public double Progress
        {
            get
            {
                return progress;
            }
            set
            {
                progress = value;
            }
        }

        public Book(string bookPath, string titlePath, string name, string progress)
        {
            this.uri = new Uri(bookPath, UriKind.Absolute);
            this.titleUri = new Uri(titlePath, UriKind.Absolute);
            this.name = name;
            char separator = Thread.CurrentThread.CurrentCulture.NumberFormat.NumberDecimalSeparator[0];
            progress = progress.Replace('.', separator);
            progress = progress.Replace(',', separator);
            this.progress = double.Parse(progress);

            Logger.Log(String.Format("Book {0} created", name));
        }

        public void UpdateInFile(string path)
        {
            Logger.Log(String.Format("Book {0} started updating", name));
            List<string> res = new List<string>();
            string[] file = System.IO.File.ReadAllLines(path);
            for (int i = 0; i < file.Length; i += 5)
            {
                string filePath = file[i];
                string titlePath = file[i + 1];
                string bookName = file[i + 2];

                char separator = Thread.CurrentThread.CurrentCulture.NumberFormat.NumberDecimalSeparator[0];
                file[i + 3] = file[i + 3].Replace('.', separator);
                file[i + 3] = file[i + 3].Replace(',', separator);

                double percentage = double.Parse(file[i + 3]);
                if (bookName != this.name)
                {
                    res.Add(filePath);
                    res.Add(titlePath);
                    res.Add(bookName);
                    res.Add(percentage.ToString());
                    res.Add("-------------------------");
                }
                else
                {
                    res.Add(filePath);
                    res.Add(titlePath);
                    res.Add(bookName);
                    res.Add(this.progress.ToString());
                    res.Add("-------------------------");
                }
            }

            file = new string[res.Count];
            for (int i = 0; i < res.Count; i++)
            {
                file[i] = res[i];
            }
            System.IO.File.WriteAllLines(path, file);
            Logger.Log(String.Format("Book {0} updated", name));
        }
    }
}
