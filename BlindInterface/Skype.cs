﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using SkypeManager;

namespace BlindInterface
{
    class Skype
    {
        static SkypeManager.Manager manager;

        static Skype()
        {
            manager = new SkypeManager.Manager();
            manager.CallFinished += manager_CallFinished;
            manager.CallMissed += manager_CallMissed;
            manager.CallRefused += manager_CallRefused;
            manager.IncomingCall += manager_IncomingCall;
            manager.CallMissed += manager_CallMissed;

            lastCallResult = CallStatus.Finished;
        }

        public static void Call(string skypeId)
        {
            manager.Call(skypeId);
        }
        public static void FinishCall()
        {
            manager.FinishCall();
        }
        public static void AnswerCall()
        {
            manager.AnswerCall();
        }
        public static void DenyCall()
        {
            manager.DenyCall();
        }
        public static void UpdateContactsOnline()
        {
            manager.UpdateContactsOnline();
        }
        public static void ResetMissedCalls()
        {
            manager.ResetMissedCalls();
        }
        public static Dictionary<string, bool> GetContactStatuses()
        {
            return manager.GetContactsStatuses();
        }
        public static Dictionary<string, bool> GetMissedCalls()
        {
            return manager.GetMissedCalls();
        }
        public static List<string> GetAllContacts()
        {
            return manager.GetContacts();
        }
        public static event EventHandler CallRefused;
        public static event EventHandler CallFinished;
        public static event EventHandler CallMissed;
        public static event EventHandler<string> IncomingCall;

        static void manager_IncomingCall(object sender, string partnerId)
        {
            if (IncomingCall != null)
            {
                IncomingCall(null, partnerId);
            }
        }
        static void manager_CallRefused(object sender, EventArgs e)
        {
            lastCallResult = CallStatus.Refused;
            if (CallRefused != null)
            {
                CallRefused(null, e);
            }
        }
        static void manager_CallMissed(object sender, EventArgs e)
        {
            lastCallResult = CallStatus.Missed;
            if (CallMissed != null)
            {
                CallMissed(null, e);
            }
        }
        static void manager_CallFinished(object sender, EventArgs e)
        {
            lastCallResult = CallStatus.Finished;
            if (CallFinished != null)
            {
                CallFinished(null, e);
            }
        }

        public static Uri GetContactUri(string contact)
        {
            return new Uri(String.Format(@"skype\{0}.wav", contact), UriKind.Relative);
        }

        static CallStatus lastCallResult;
        public static CallStatus LastCallResult
        {
            get
            {
                return lastCallResult;
            }
        }
    }
}
