﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SkypeManager.SkypeResponces
{
    class CallMissedResponce : CallStatusResponce
    {
        public CallMissedResponce(int id)
        {
            this.id = id;
            this.status = "call missed";
            this.callStatus = SkypeManager.CallStatus.Missed;
        }

        public static bool TryParse(string responce, out SkypeResponce result)
        {
            return CallStatusResponce.TryParse(responce, "MISSED", out result, typeof(CallMissedResponce));
        }
    }
}
