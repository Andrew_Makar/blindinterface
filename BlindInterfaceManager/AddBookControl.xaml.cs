﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Microsoft.Win32;
using NAudio;
using NAudio.Mixer;
using NAudio.Wave;

namespace BlindInterfaceManager
{
    /// <summary>
    /// Interaction logic for AddBookControl.xaml
    /// </summary>
    public partial class AddBookControl : UserControl
    {
        WaveIn waveIn;
        UnsignedMixerControl volumeControl;
        WaveFormat format;
        WaveFileWriter writer;

        public ViewBooksControl ViewBooks
        {
            get;
            set;
        }

        public AddBookControl()
        {
            InitializeComponent();

            grid.DataContext = this;


            var mixerLine = new MixerLine((IntPtr)0, 0, MixerFlags.WaveIn);
            foreach (var control in mixerLine.Controls)
            {
                if (control.ControlType == MixerControlType.Volume)
                {
                    volumeControl = control as UnsignedMixerControl;
                    break;
                }
            }

            waveIn = new WaveIn();
            waveIn.DeviceNumber = 0;
            waveIn.DataAvailable += waveIn_DataAvailable;
            int sampleRate = 8000;
            int channels = 1;
            format = new WaveFormat(sampleRate, channels);
            waveIn.WaveFormat = format;
            waveIn.StartRecording();
        }

        int cnt = 0;
        double m;

        void waveIn_DataAvailable(object sender, WaveInEventArgs e)
        {
            if (isRecording)
            {
                writer.Write(e.Buffer, 0, e.BytesRecorded);
            }

            for (int i = 0; i < e.BytesRecorded; i += 2)
            {
                short s = (short)((e.Buffer[i + 1] << 8) | e.Buffer[i]);
                double s2 = s / (double)(1 << 15);
                cnt++;
                m = Math.Max(m, s2);
                if (cnt >= 100)
                {
                    cnt = 0;
                    progressBar.Value = m * 100;
                    m = 0;
                }
            }
        }

        private void Slider_ValueChanged_1(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            volumeControl.Percent = e.NewValue;
        }

        bool isRecording = false;

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            if (!isRecording)
            {
                writer = new WaveFileWriter(System.AppDomain.CurrentDomain.BaseDirectory + "temp.wav", format);
                isRecording = true;
                record.Content = "Зупинити запис";
                listen.IsEnabled = false;
                add.IsEnabled = false;
                open.IsEnabled = false;
                bookName.IsEnabled = false;
                path.IsEnabled = false;
            }
            else
            {
                isRecording = false;
                writer.Close();
                record.Content = "Записати ще раз";
                listen.IsEnabled = true;
                add.IsEnabled = true;
                open.IsEnabled = true;
                bookName.IsEnabled = true;
                path.IsEnabled = true;
            }
        }
        MediaPlayer player;
        private void listen_Click_1(object sender, RoutedEventArgs e)
        {
            listen.IsEnabled = false;
            record.IsEnabled = false;
            player = new MediaPlayer();
            player.MediaEnded += player_MediaEnded;
            player.MediaFailed += player_MediaEnded;
            player.Open(new Uri(System.AppDomain.CurrentDomain.BaseDirectory + "temp.wav", UriKind.Relative));
            player.Play();
        }

        void player_MediaEnded(object sender, EventArgs e)
        {
            player.Close();
            listen.IsEnabled = true;
            record.IsEnabled = true;
        }


        string bookFileName;
        public string BookPath
        {
            get { return (string)GetValue(BookPathProperty); }
            set { SetValue(BookPathProperty, value); }
        }

        // Using a DependencyProperty as the backing store for BookPath.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty BookPathProperty =
            DependencyProperty.Register("BookPath", typeof(string), typeof(AddBookControl), new PropertyMetadata(""));



        private void Button_Click_2(object sender, RoutedEventArgs e)
        {
            if (Book.ContainsBook(System.AppDomain.CurrentDomain.BaseDirectory + "books.txt", bookName.Text))
            {
                MessageBox.Show("Така книжка вже добавлена", "Помилка", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }

            //Debugger.Break();
            string baseDirectory = AppDomain.CurrentDomain.BaseDirectory;
            string filePath = baseDirectory + @"books\" + bookFileName;
            string titleFileName = bookFileName;
            while (titleFileName[titleFileName.Length - 1] != '.')
            {
                titleFileName = titleFileName.Remove(titleFileName.Length - 1);
            }
            titleFileName += "wav";

            try
            {
                System.IO.File.Copy(BookPath, filePath);
            }
            catch (Exception)
            {
            }

            string titlePath = baseDirectory + @"titles\" + titleFileName;
            try
            {
                System.IO.File.Copy(System.AppDomain.CurrentDomain.BaseDirectory + "temp.wav", titlePath);
            }
            catch (Exception)
            {
                for (int i = 2; ; i++)
                {
                    titlePath = baseDirectory+@"titles\("+i.ToString()+")"+titleFileName;
                    if (!System.IO.File.Exists(titlePath))
                    {
                        break;
                    }
                }
                System.IO.File.Copy(baseDirectory + "temp.wav", titlePath);
            }

            Book book = new Book(filePath, titlePath, bookName.Text);
            book.AddToFile(System.AppDomain.CurrentDomain.BaseDirectory + "books.txt");

            if (ViewBooks != null)
            {
                ViewBooks.SetBooks();
            }

            add.IsEnabled = false;
            listen.IsEnabled = false;
            record.Content = "Записати";
            path.Text = "";
            bookName.Text = "";

            MessageBox.Show("Книжку успішно добавлено", "Успіх", MessageBoxButton.OK, MessageBoxImage.Asterisk);
        }

        private void Button_Click_3(object sender, RoutedEventArgs e)
        {
            OpenFileDialog dialog = new OpenFileDialog();
         //   dialog.Filter = "*.mp3|*.mp3";
            if (dialog.ShowDialog() ?? false)
            {
                BookPath = dialog.FileName;
                bookName.Text = dialog.SafeFileName;
                bookFileName = dialog.SafeFileName;
            }

        }


        private void TextBox_GotFocus_1(object sender, RoutedEventArgs e)
        {
            Button_Click_3(null, null);
        }
    }
}
