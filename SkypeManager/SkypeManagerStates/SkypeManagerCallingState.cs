﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Threading;
using SkypeManager.SkypeResponces;

namespace SkypeManager.SkypeManagerStates
{
    class SkypeManagerCallingState : ISkypeManagerState
    {
        int callId;
        Manager manager;
        bool shouldBeStopped;
        DispatcherTimer timer;

        public SkypeManagerCallingState(int callId, Manager manager)
        {
            this.callId = callId;
            this.manager = manager;

            timer = new DispatcherTimer();
            timer.Interval = new TimeSpan(0, 0, 1);
            timer.Tick += timer_Tick;
        }

        void timer_Tick(object sender, EventArgs e)
        {
            CheckCallEnding();
        }

        public void Enter()
        {
            shouldBeStopped = false;
            //CheckCallEnding();
            timer.Start();
        }
        public void Leave()
        {

        }

        public void Call(string skypeId)
        {
            throw new Exception("Can't call when call is on");
        }
        public void FinishCall()
        {
            string command = "SET CALL " + callId.ToString() + " STATUS FINISHED";
            manager.QueryCommand(command, (responce) =>
            {
               // manager.State = new SkypeManagerWaitingState(manager);
               // manager.State.Enter();
            });

        }
        public void AnswerCall()
        {
            throw new Exception("Can't answer call when call is on");
        }
        public void DenyCall()
        {
            throw new Exception("Can't deny call when call is on");
        }

        private void CheckCallEnding()
        {
            timer.Stop();
            string command = "GET CALL " + callId.ToString() + " STATUS";
            manager.QueryCommand(command, (responce) =>
            {
                CallStatus status = (responce as CallStatusResponce).CallStatus;

                if (status == CallStatus.Refused || status == CallStatus.Finished || status==CallStatus.Missed)
                {
               //     timer.Stop();

                    manager.State = new SkypeManagerWaitingState(manager);
                    manager.State.Enter();

                    if (status == CallStatus.Refused)
                    {
                        manager.OnCallRefused();
                    }
                    else if (status == CallStatus.Finished)
                    {
                        manager.OnCallFinished();
                    }
                    else if (status == CallStatus.Missed)
                    {
                        manager.OnCallMissed();
                    }

                }
                else
                {
                    timer.Start();
                }
            });
        }



    }
}
