﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Windows.Forms;
using BlindInterface.Actions;
using BlindInterface.Controllers;
using Action = BlindInterface.Actions.Action;

namespace BlindInterface.States
{
    class StateFactory
    {
        static State startState;
        public static State CreateStartState()
        {
            if (startState != null)
            {
                return startState;
            }
            StateCreator creator = new StateCreator("Start state");
            startState = creator;

            creator.EnterAction = ActionFactory.CreateMultiAction(ActionFactory.CreateTextAction(TextMessages.StartEntry),
                                                                    ActionFactory.CreateAudioAction(AudioUri.StartEntry));

            creator.DoAction = ActionFactory.CreateRedirectionAction(StateFactory.CreateMainState());
            //if (BookProvider.Count == 0)
            //{
            //    creator.DoAction = ActionFactory.CreateRedirectionAction(StateFactory.CreateNoBooksState());
            //}
            //else
            //{
            //    creator.DoAction = ActionFactory.CreateRedirectionAction(StateFactory.CreateMainState());
            //}
            creator.LeaveAction = ActionFactory.CreateEmptyAction();
            creator.Controller = null;

            return startState;
        }

        static State mainState;
        public static State CreateMainState()
        {
            if (mainState != null)
            {
                return mainState;
            }

            StateCreator creator = new StateCreator("Main state");
            mainState = creator;

            creator.EnterAction = ActionFactory.CreateMultiAction(ActionFactory.CreateTextAction(TextMessages.MainEntry),
                                                                    ActionFactory.CreateAudioAction(AudioUri.MainEntry));
            creator.DoAction = ActionFactory.CreateReEnterAction();
            creator.LeaveAction = ActionFactory.CreateEmptyAction();
            if (BookProvider.Count != 0)
            {
                creator.Controller = ControllerFactory.CreateRedirectionController(
                                        StateFactory.CreateChooseBookState(0),
                                        StateFactory.CreateMissedCallsState());
            }
            else
            {
                creator.Controller = ControllerFactory.CreateRedirectionController(
                                        StateFactory.CreateNoBooksState(),
                                        StateFactory.CreateMissedCallsState());
            }

            return mainState;
        }

        static State booksState;
        public static State CreateBooksState()
        {
            if (booksState != null)
            {
                return booksState;
            }

            StateCreator creator = new StateCreator("Books state");
            booksState = creator;

            creator.EnterAction = ActionFactory.CreateMultiAction(ActionFactory.CreateTextAction(TextMessages.BooksEntry),
                                                                    ActionFactory.CreateAudioAction(AudioUri.BookEntry));
            if (BookProvider.Count == 0)
            {
                creator.DoAction = ActionFactory.CreateRedirectionAction(StateFactory.CreateNoBooksState());
            }
            else
            {
                creator.DoAction = ActionFactory.CreateRedirectionAction(StateFactory.CreateChooseBookState(0));
            }
            creator.LeaveAction = ActionFactory.CreateEmptyAction();
            creator.Controller = null;
            return booksState;
        }

        static List<State> chooseBookState = new List<State>();
        public static State CreateChooseBookState(int index)
        {
            index %= BookProvider.Count;

            while (chooseBookState.Count <= index)
            {
                chooseBookState.Add(null);
            }
            if (chooseBookState[index] != null)
            {
                return chooseBookState[index];
            }

            StateCreator creator = new StateCreator(String.Format("Choose book state # {0}", index));
            chooseBookState[index] = creator;

            creator.EnterAction = ActionFactory.CreateMultiAction(
                                    ActionFactory.CreateTextAction(
                                        TextMessages.ChooseBookEntry1 +
                                        BookProvider.GetBookName(index) +
                                        TextMessages.ChooseBookEntry2),
                                    ActionFactory.CreateAudioAction(
                                        AudioUri.ChooseBookEntry1,
                                        BookProvider.GetBookTitleUri(index),
                                        AudioUri.ChooseBookEntry2));

            creator.DoAction = ActionFactory.CreateReEnterAction();
            creator.LeaveAction = ActionFactory.CreateEmptyAction();
            creator.Controller = ControllerFactory.CreateRedirectionController(StateFactory.CreateReadBookState(index),
                                                                               StateFactory.CreateChooseBookState(index + 1));

            return chooseBookState[index];
        }

        static List<State> readBookState = new List<State>();
        public static State CreateReadBookState(int index)
        {
            index %= BookProvider.Count;
            while (readBookState.Count <= index)
            {
                readBookState.Add(null);
            }
            if (readBookState[index] != null)
            {
                return readBookState[index];
            }

            StateCreator creator = new StateCreator(String.Format("Read book state # {0}", index));
            readBookState[index] = creator;

            creator.EnterAction = ActionFactory.CreateMultiAction(ActionFactory.CreateTextAction(TextMessages.ReadBookEntry1 +
                                                                                                    BookProvider.GetBookName(index) +
                                                                                                    TextMessages.ReadBookEntry2),
                                                                    ActionFactory.CreateAudioAction(AudioUri.ReadBookEntry1,
                                                                                                    BookProvider.GetBookTitleUri(index),
                                                                                                    AudioUri.ReadBookEntry2));
            creator.DoAction = ActionFactory.CreateRedirectionAction(StateFactory.CreateActualReadBookState(index));
            creator.LeaveAction = ActionFactory.CreateEmptyAction();
            creator.Controller = null;

            return readBookState[index];
        }

        static List<State> actualReadBook = new List<State>();
        public static State CreateActualReadBookState(int index)
        {
            index %= BookProvider.Count;
            while (actualReadBook.Count <= index)
            {
                actualReadBook.Add(null);
            }
            if (actualReadBook[index] != null)
            {
                return actualReadBook[index];
            }

            StateCreator creator = new StateCreator(String.Format("Actual read book state # {0}", index));
            actualReadBook[index] = creator;

            creator.EnterAction = ActionFactory.CreateMultiAction(ActionFactory.CreateTextAction(TextMessages.ActualReadBookEntry +
                                                                                                    BookProvider.GetBookName(index)),
                                                                    ActionFactory.CreateAudioBookAction(BookProvider.GetBookUri(index),
                                                                                                           BookProvider.GetBookProgress(index),
                                                                                                           index));
            creator.DoAction = ActionFactory.CreateRedirectionAction(StateFactory.CreateEndOfBookState(index));
            creator.LeaveAction = ActionFactory.CreateEmptyAction();
            creator.Controller = ControllerFactory.CreateRedirectionController(StateFactory.CreatePauseState(index), null);

            creator.AfterInterruptionState = StateFactory.CreatePauseState(index);

            return actualReadBook[index];
        }

        static List<State> pauseState = new List<State>();
        public static State CreatePauseState(int index)
        {
            index %= BookProvider.Count;
            while (pauseState.Count <= index)
            {
                pauseState.Add(null);
            }
            if (pauseState[index] != null)
            {
                return pauseState[index];
            }

            StateCreator creator = new StateCreator(String.Format("Pause state # {0}", index));
            pauseState[index] = creator;

            creator.EnterAction = ActionFactory.CreateMultiAction(ActionFactory.CreateTextAction(TextMessages.PauseEntry),
                                                                    ActionFactory.CreateAudioAction(AudioUri.PauseEntry));
            creator.DoAction = ActionFactory.CreateReEnterAction();
            creator.LeaveAction = ActionFactory.CreateEmptyAction();
            creator.Controller = ControllerFactory.CreateRedirectionController(StateFactory.CreateActualReadBookState(index),
                                                                                StateFactory.CreateMainState());

            return pauseState[index];
        }

        static List<State> endOfBookState = new List<State>();
        public static State CreateEndOfBookState(int index)
        {
            index %= BookProvider.Count;
            while (endOfBookState.Count <= index)
            {
                endOfBookState.Add(null);
            }
            if (endOfBookState[index] != null)
            {
                return endOfBookState[index];
            }

            StateCreator creator = new StateCreator("End of book state");
            endOfBookState[index] = creator;

            creator.EnterAction = ActionFactory.CreateMultiAction(ActionFactory.CreateTextAction(TextMessages.EndOfBookEntry1 +
                                                                                                    BookProvider.GetBookName(index) +
                                                                                                    TextMessages.EndOfBookEntry2),
                                                                    ActionFactory.CreateAudioAction(AudioUri.EndOfBookEntry1,
                                                                                                    BookProvider.GetBookTitleUri(index),
                                                                                                    AudioUri.EndOfBookEntry2));
            creator.DoAction = ActionFactory.CreateReEnterAction();
            creator.LeaveAction = ActionFactory.CreateEmptyAction();
            creator.Controller = ControllerFactory.CreateRedirectionController(StateFactory.CreateMainState(), null);

            return endOfBookState[index];
        }

        static State noBooksState;
        public static State CreateNoBooksState()
        {
            if (noBooksState != null)
            {
                return noBooksState;
            }

            StateCreator creator = new StateCreator("No book state");
            noBooksState = creator;

            creator.EnterAction = ActionFactory.CreateMultiAction(ActionFactory.CreateTextAction(TextMessages.NoBooksEntry),
                                                                    ActionFactory.CreateAudioAction(AudioUri.NoBooksEntry));
            creator.DoAction = ActionFactory.CreateReEnterAction();
            creator.LeaveAction = ActionFactory.CreateEmptyAction();
            creator.Controller = ControllerFactory.CreateRedirectionController(StateFactory.CreateMainState(), null);

            return noBooksState;
        }

        //////////////////Skype/////////////////////////

        static State missedCallsState;
        public static State CreateMissedCallsState()
        {
            if (missedCallsState != null)
            {
                return missedCallsState;
            }

            StateCreator creator = new StateCreator("Missed calls state");
            missedCallsState = creator;

            List<Action> enterActions = new List<Action>();
            enterActions.Add(
                ActionFactory.CreateConditionAction(
                    new Func<bool>(() =>
                    {
                        var missedCalls = Skype.GetMissedCalls();
                        foreach (var t in missedCalls)
                        {
                            if (t.Value == true)
                            {
                                return true;
                            }
                        }
                        return false;
                    }),
                    ActionFactory.CreateEmptyAction(),
                    ActionFactory.CreateRedirectionAction(
                        StateFactory.CreateAvailableContactsState())));

            enterActions.Add(
                ActionFactory.CreateTextAction(
                    TextMessages.MissedCallsEntry));

            enterActions.Add(
                ActionFactory.CreateAudioAction(
                    AudioUri.MissedCallsEntry));

            List<string> contacts = Skype.GetAllContacts();

            for (int i = 0; i < contacts.Count; i++)
            {
                string skypeId = contacts[i];

                enterActions.Add(
                    ActionFactory.CreateConditionAction(
                        new Func<bool>(() =>
                        {
                            var missedCalls = Skype.GetMissedCalls();
                            return missedCalls[skypeId];
                        }),
                        ActionFactory.CreateAudioAction(
                            Skype.GetContactUri(skypeId))));
            }
            enterActions.Add(
                ActionFactory.CreateResetMissedCallsAction());

            creator.EnterAction = ActionFactory.CreateMultiAction(enterActions);

            creator.DoAction = ActionFactory.CreateRedirectionAction(
                                StateFactory.CreateAvailableContactsState());

            creator.LeaveAction = ActionFactory.CreateEmptyAction();

            creator.Controller = null;

            return missedCallsState;
        }

        static State availableContactsState;
        public static State CreateAvailableContactsState()
        {
            if (availableContactsState != null)
            {
                return availableContactsState;
            }

            StateCreator creator = new StateCreator("Available contacts status");
            availableContactsState = creator;

            List<Action> contactsAction = new List<Action>();
            contactsAction.Add(ActionFactory.CreateTextAction(TextMessages.AvailableContactsEntry));
            contactsAction.Add(ActionFactory.CreateAudioAction(AudioUri.AvailableContactsEntry1));

            List<string> contacts = Skype.GetAllContacts();
            foreach (var t in contacts)
            {
                string contact = t;
                contactsAction.Add(ActionFactory.CreateConditionAction(
                                    new Func<bool>(() =>
                                    {
                                        Dictionary<string, bool> statuses = Skype.GetContactStatuses();
                                        return statuses[contact];
                                    }),
                                    ActionFactory.CreateAudioAction(Skype.GetContactUri(contact))));
            }
            contactsAction.Add(ActionFactory.CreateAudioAction(AudioUri.AvailableContactsEntry2));



            creator.EnterAction = ActionFactory.CreateConditionAction(
                                    new Func<bool>(() =>
                                    {
                                        Skype.UpdateContactsOnline();

                                        Dictionary<string, bool> statuses = Skype.GetContactStatuses();

                                        foreach (var t in statuses)
                                        {
                                            if (t.Value) return true;
                                        }
                                        return false;
                                    }),
                                    ActionFactory.CreateMultiAction(contactsAction),
                                    ActionFactory.CreateRedirectionAction(
                                        StateFactory.CreateAvailableContactsNoContactsState()));

            creator.DoAction = ActionFactory.CreateReEnterAction();
            creator.LeaveAction = ActionFactory.CreateEmptyAction();
            creator.Controller = ControllerFactory.CreateRedirectionController(
                                    StateFactory.CreateChooseContactState(0),
                                    StateFactory.CreateMainState());

            return availableContactsState;

        }

        static State availableContactsNoContactsState;
        public static State CreateAvailableContactsNoContactsState()
        {
            if (availableContactsNoContactsState != null)
            {
                return availableContactsNoContactsState;
            }
            StateCreator creator = new StateCreator("Available contacts no contacts state");
            availableContactsNoContactsState = creator;

            creator.EnterAction = ActionFactory.CreateMultiAction(
                                    ActionFactory.CreateTextAction(
                                        TextMessages.AvailableContactsNoContactsEntry),
                                    ActionFactory.CreateAudioAction(
                                        AudioUri.AvailableContactsNoContactsEntry));

            creator.DoAction = ActionFactory.CreateRedirectionAction(StateFactory.CreateMainState());
            creator.LeaveAction = ActionFactory.CreateEmptyAction();
            creator.Controller = null;

            return availableContactsNoContactsState;
        }

        static List<State> chooseContactState = new List<State>();
        public static State CreateChooseContactState(int index)
        {
            if (index == Skype.GetAllContacts().Count)
            {
                return StateFactory.CreateAvailableContactsState();
            }

            while (chooseContactState.Count <= index)
            {
                chooseContactState.Add(null);
            }
            if (chooseContactState[index] != null)
            {
                return chooseContactState[index];
            }

            StateCreator creator = new StateCreator(String.Format("Choose contact state #{0}", index));
            chooseContactState[index] = creator;

            string contact = Skype.GetAllContacts()[index];

            creator.EnterAction = ActionFactory.CreateConditionAction(
                                    new Func<bool>(() => Skype.GetContactStatuses()[contact]),
                                    ActionFactory.CreateMultiAction(
                                        ActionFactory.CreateTextAction(
                                            TextMessages.ChooseContactEntry +
                                            contact),
                                        ActionFactory.CreateAudioAction(
                                            AudioUri.ChooseContactEntry,
                                            Skype.GetContactUri(contact)))/*,
                                    ActionFactory.CreateRedirectionAction(
                                        StateFactory.CreateChooseContactState(index+1))*/);

            creator.DoAction = ActionFactory.CreateConditionAction(
                                new Func<bool>(() => Skype.GetContactStatuses()[contact]),
                                ActionFactory.CreateReEnterAction(),
                                ActionFactory.CreateRedirectionAction(
                                    StateFactory.CreateChooseContactState(index + 1)));

            creator.LeaveAction = ActionFactory.CreateEmptyAction();

            creator.Controller = ControllerFactory.CreateRedirectionController(
                                    StateFactory.CreateCallState(index),
                                    StateFactory.CreateChooseContactState(index + 1));

            return chooseContactState[index];
        }

        static List<State> callState = new List<State>();
        public static State CreateCallState(int index)
        {
            while (callState.Count <= index)
            {
                callState.Add(null);
            }
            if (callState[index] != null)
            {
                return callState[index];
            }

            StateCreator creator = new StateCreator(String.Format("Call state #{0}", index));
            callState[index] = creator;

            creator.EnterAction = ActionFactory.CreateMultiAction(
                                    ActionFactory.CreateTextAction(
                                        TextMessages.CallEntry1 +
                                        Skype.GetAllContacts()[index] +
                                        TextMessages.CallEntry2),
                                    ActionFactory.CreateAudioAction(
                                        AudioUri.CallEntry1,
                                        Skype.GetContactUri(Skype.GetAllContacts()[index]),
                                        AudioUri.CallEntry2));

            creator.DoAction = ActionFactory.CreateRedirectionAction(StateFactory.CreateActualCallState(index));

            creator.LeaveAction = ActionFactory.CreateEmptyAction();
            creator.Controller = null;

            return callState[index];
        }

        static List<State> actualCallState = new List<State>();
        public static State CreateActualCallState(int index)
        {
            while (actualCallState.Count <= index)
            {
                actualCallState.Add(null);
            }
            if (actualCallState[index] != null)
            {
                return actualCallState[index];
            }

            StateCreator creator = new StateCreator(String.Format("Actual call state #{0}", index));
            actualCallState[index] = creator;

            creator.EnterAction = ActionFactory.CreateCallAction(Skype.GetAllContacts()[index]);
            creator.DoAction = ActionFactory.CreateConditionAction(
                                    new Func<bool>(() =>
                                    {
                                        return Skype.LastCallResult == SkypeManager.CallStatus.Finished;
                                    }),
                                    ActionFactory.CreateRedirectionAction(
                                        StateFactory.CreateCallFinishedState()),
                                    ActionFactory.CreateRedirectionAction(
                                        StateFactory.CreateCallDeniedState()));

            creator.LeaveAction = ActionFactory.CreateEmptyAction();

            creator.Controller = ControllerFactory.CreateActionController(
                                    ActionFactory.CreateMultiAction(
                                        ActionFactory.CreateFinishCallAction(),
                                        ActionFactory.CreateRedirectionAction(
                                            StateFactory.CreateCallFinishedState())),
                                    null);
            creator.IsInterruptable = false;

            return actualCallState[index];
        }

        static State callDeniedState;
        public static State CreateCallDeniedState()
        {
            if (callDeniedState != null)
            {
                return callDeniedState;
            }

            StateCreator creator = new StateCreator("Call denied state");
            callDeniedState = creator;

            creator.EnterAction = ActionFactory.CreateMultiAction(
                                    ActionFactory.CreateTextAction(
                                        TextMessages.CallDeniedEntry),
                                    ActionFactory.CreateAudioAction(
                                        AudioUri.CallDeniedEntry));

            creator.DoAction = ActionFactory.CreateRedirectionAction(
                                StateFactory.CreateMainState());

            creator.LeaveAction = ActionFactory.CreateEmptyAction();

            creator.Controller = null;

            return callDeniedState;
        }

        static State callFinishedState;
        public static State CreateCallFinishedState()
        {
            if (callFinishedState != null)
            {
                return callFinishedState;
            }

            StateCreator creator = new StateCreator("Call finished state");
            callFinishedState = creator;

            creator.EnterAction = ActionFactory.CreateMultiAction(
                                    ActionFactory.CreateTextAction(
                                        TextMessages.CallFinishedEntry),
                                    ActionFactory.CreateAudioAction(
                                        AudioUri.CallFinishedEntry));

            creator.DoAction = ActionFactory.CreateRedirectionAction(
                                StateFactory.CreateMainState());

            creator.LeaveAction = ActionFactory.CreateEmptyAction();

            creator.Controller = null;

            return callFinishedState;
        }

        // Incoming call

        static List<State> incomingCallState = new List<State>();
        public static State CreateIncomingCallState(int index)
        {
            while (incomingCallState.Count <= index)
            {
                incomingCallState.Add(null);
            }
            if (incomingCallState[index] != null)
            {
                return incomingCallState[index];
            }

            StateCreator creator = new StateCreator("Incoming call state");
            incomingCallState[index] = creator;

            string skypeId = Skype.GetAllContacts()[index];

            creator.EnterAction = ActionFactory.CreateSimultaneousAction(
                                    ActionFactory.CreateEventRedirectionAction(
                                        typeof(Skype),
                                        "CallFinished",
                                        StateFactory.CreateContactFinishedIncomingCall()),
                                    ActionFactory.CreateMultiAction(
                                        ActionFactory.CreateTextAction(
                                            TextMessages.IncomingCallEntry),
                                        ActionFactory.CreateAudioAction(
                                            AudioUri.IncomingCallEntry1,
                                            Skype.GetContactUri(skypeId),
                                            AudioUri.IncomingCallEntry2)));

            creator.DoAction = ActionFactory.CreateSimultaneousAction(
                                ActionFactory.CreateEventRedirectionAction(
                                    typeof(Skype),
                                    "CallFinished",
                                    StateFactory.CreateContactFinishedIncomingCall()),
                                ActionFactory.CreateReEnterAction());

            creator.LeaveAction = ActionFactory.CreateEmptyAction();

            creator.Controller = ControllerFactory.CreateRedirectionController(
                                    StateFactory.CreateActualIncomingCall(),
                                    StateFactory.CreateDenyIncomingCall());

            creator.IsInterruptable = false;

            return incomingCallState[index];
        }

        static State contactFinishedIncomingCall;
        public static State CreateContactFinishedIncomingCall()
        {
            if (contactFinishedIncomingCall != null)
            {
                return contactFinishedIncomingCall;
            }

            StateCreator creator = new StateCreator("Contact finished incoming call state");
            contactFinishedIncomingCall = creator;

            creator.EnterAction = ActionFactory.CreateMultiAction(
                                ActionFactory.CreateTextAction(
                                    TextMessages.ContactFinishedIncomingCallEntry),
                                ActionFactory.CreateAudioAction(
                                    AudioUri.ContactFinishedIncomingCallEntry));

            creator.DoAction = ActionFactory.CreateFinishSubroutineAction();

            creator.LeaveAction = ActionFactory.CreateEmptyAction();

            creator.IsInterruptable = false;

            creator.Controller = null;

            return contactFinishedIncomingCall;
        }

        static State actualIncomingCall;
        public static State CreateActualIncomingCall()
        {
            if (actualIncomingCall != null)
            {
                return actualIncomingCall;
            }

            StateCreator creator = new StateCreator("Actual incoming call state");
            actualIncomingCall = creator;

            creator.EnterAction = ActionFactory.CreateMultiAction(
                                    ActionFactory.CreateTextAction(
                                        TextMessages.ActualIncomingCallEntry),
                                    ActionFactory.CreateAnswerCallAction());

            creator.DoAction = ActionFactory.CreateRedirectionAction(
                                StateFactory.CreateEndIncomingCall());

            creator.LeaveAction = ActionFactory.CreateEmptyAction();

            creator.Controller = ControllerFactory.CreateActionController(
                                    ActionFactory.CreateMultiAction(
                                        ActionFactory.CreateFinishCallAction(),
                                        ActionFactory.CreateRedirectionAction(
                                            StateFactory.CreateEndIncomingCall())),
                                    null);

            creator.IsInterruptable = false;

            return actualIncomingCall;

        }

        static State denyIncomingCall;
        public static State CreateDenyIncomingCall()
        {
            if (denyIncomingCall != null)
            {
                return denyIncomingCall;
            }

            StateCreator creator = new StateCreator("Deny incoming call");
            denyIncomingCall = creator;

            creator.EnterAction = ActionFactory.CreateMultiAction(
                                    ActionFactory.CreateDenyCallAction(),
                                    ActionFactory.CreateTextAction(
                                        TextMessages.DenyIncomingCallEntry),
                                    ActionFactory.CreateAudioAction(
                                        AudioUri.DenyIncomingCallEntry));

            creator.DoAction = ActionFactory.CreateFinishSubroutineAction();

            creator.LeaveAction = ActionFactory.CreateEmptyAction();

            creator.Controller = null;

            creator.IsInterruptable = false;

            return denyIncomingCall;
        }

        static State endIncomingCall;
        public static State CreateEndIncomingCall()
        {
            if (endIncomingCall != null)
            {
                return endIncomingCall;
            }

            StateCreator creator = new StateCreator("End incoming call");
            endIncomingCall = creator;

            creator.EnterAction = ActionFactory.CreateMultiAction(
                                    ActionFactory.CreateTextAction(
                                        TextMessages.EndIncomingCallEntry),
                                    ActionFactory.CreateAudioAction(
                                        AudioUri.EndIncomingCallEntry));

            creator.DoAction = ActionFactory.CreateFinishSubroutineAction();

            creator.LeaveAction = ActionFactory.CreateEmptyAction();

            creator.Controller = null;

            creator.IsInterruptable = false;

            return endIncomingCall;
        }

    }
}
