﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using BlindInterface.Actions;
using BlindInterface.Controllers;
using Action = BlindInterface.Actions.Action;

namespace BlindInterface.States
{
    class State
    {
        protected Action enterAction;
        protected Action doAction;
        protected Action leaveAction;
        protected State afterInterruptionState;
        protected bool isInterruptable;

        public State AfterInterruptionState
        {
            get
            {
                return afterInterruptionState;
            }
        }
        public bool IsInterruptable
        {
            get
            {
                return isInterruptable;
            }
        }

        protected string name;
        public string Name
        {
            get
            {
                return name;
            }
        }

        public State(Action enterAction, Action doAction, Action leaveAction, Controller controller, string name)
        {
            this.name = name;

            this.isInterruptable = true;
            this.afterInterruptionState = this;

            this.enterAction = enterAction;
            this.doAction = doAction;
            this.leaveAction = leaveAction;

            this.number = count;
            count++;

            Logger.Log(String.Format("State \"{0}\" created.", this.name));
        }

        public void Enter()
        {
            Logger.Log(String.Format("State \"{0}\" Enter.", this.name));

            enterAction.ActionEnded -= Do;

            enterAction.ActionEnded += Do;
            enterAction.Do();
        }
        private void Do()
        {
            Logger.Log(String.Format("State \"{0}\" Do.", this.name));

            

            enterAction.ActionEnded -= Do;
            doAction.Do();
        }
        public void Leave()
        {
            Logger.Log(String.Format("State \"{0}\" Leave.", this.name));

            leaveAction.Do();
        }

        protected Controller controller;

        public void Action1()
        {
            Logger.Log(String.Format("State \"{0}\" Action1", this.name));

            if (controller != null)
            {
                controller.Action1();
            }
        }
        public void Action2()
        {
            Logger.Log(String.Format("State \"{0}\" Action2", this.name));

            if (controller != null)
            {
                controller.Action2();
            }
        }

        static int count = 0;
        int number;

        public override string ToString()
        {
            return number.ToString();
        }
    }
}
