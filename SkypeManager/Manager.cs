﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;

using SkypeControl;
using SkypeManager.SkypeResponces;
using SkypeManager.SkypeManagerStates;
using System.Windows.Threading;

namespace SkypeManager
{
    public class Manager
    {
        object locker = new object();
        SkypeProxy skype;
        Dictionary<string, Action<SkypeResponce>> actions;
        Dictionary<string, SkypeResponce> responces;
        ISkypeManagerState state;
        public ISkypeManagerState State
        {
            get
            {
                return state;
            }
            set
            {
                state = value;
            }
        }
        bool isConnected;

        public Manager()
        {
            state = new SkypeManagerWaitingState(this);

            responces = new Dictionary<string, SkypeResponce>();
            actions = new Dictionary<string, Action<SkypeResponce>>();

            isConnected = false;

            skype = new SkypeProxy();
            skype.SkypeResponse += skype_SkypeResponse;
            skype.SkypeAttach += skype_SkypeAttach;
            skype.Conect();
            

            DispatcherTimer timer = new DispatcherTimer();
            timer.Interval = new TimeSpan(0, 0, 1);
            timer.Tick += timer_Tick;
            timer.Start();

            DispatcherTimer timer2 = new DispatcherTimer();
            timer2.Interval = new TimeSpan(0, 0, 2);
            timer2.Tick += timer2_Tick;
            timer2.Start();

            DispatcherTimer timer3 = new DispatcherTimer();
            timer3.Interval = new TimeSpan(0, 0, 2);
            timer3.Tick += timer3_Tick;
            timer3.Start();

            state = new SkypeManagerWaitingState(this);
            state.Enter();
        }

        private bool IsAvailable()
        {
            if (!isConnected)
            {
                return false;
            }
            if (!Helper.IsInternetConnected())
            {
                return false;
            }
            return true;
        }

        void timer3_Tick(object sender, EventArgs e)
        {
            skype.Conect();
        }

        void skype_SkypeAttach(object theSender, SkypeAttachEventArgs theEventArgs)
        {
            if (theEventArgs.AttachStatus == SkypeAttachStatus.Success)
            {
                isConnected = true;
            }
            if (theEventArgs.AttachStatus == SkypeAttachStatus.NotAvailable)
            {
                isConnected = false;
            }
        }

        public void UpdateContactsOnline()
        {
            //actualOnlineContacts = onlineContacts;
            if (actualOnlineContacts == null)
            {
                actualOnlineContacts = new Dictionary<string, bool>();
                foreach (var t in onlineContacts)
                {
                    actualOnlineContacts.Add(t.Key, t.Value);
                }
            }

            foreach (var t in onlineContacts)
            {
                actualOnlineContacts[t.Key] = t.Value;
            }

        }

        private void UpdateContactsOnlinePrivate()
        {
            if (onlineContacts == null)
            {
                onlineContacts = new Dictionary<string, bool>();
                List<string> contacts = this.GetContacts();
                foreach (var c in contacts)
                {
                    onlineContacts.Add(c, false);
                }
            }

            if (!IsAvailable())
            {
                foreach (var t in GetContacts())
                {
                    onlineContacts[t] = false;
                }
                return;
            }

            foreach (var c in onlineContacts)
            {
                string command = "GET USER " + c.Key + " ONLINESTATUS";
                this.QueryCommand(command, (responce) =>
                {
                    SkypeOnlineStatus status = (responce as UserOnlinestatusResponce).Status;
                    string handle = (responce as UserOnlinestatusResponce).Handle;

                    switch (status)
                    {
                        case SkypeOnlineStatus.Online:
                        case SkypeOnlineStatus.Away:
                            onlineContacts[handle] = true;
                            break;
                        default:
                            onlineContacts[handle] = false;
                            break;
                    }
                });
            }
        }

        Dictionary<string, bool> missedCalls;
        public Dictionary<string, bool> GetMissedCalls()
        {
            return missedCalls;
        }

        private void UpdateMissedCalls()
        {
            if (missedCalls == null)
            {
                missedCalls = new Dictionary<string, bool>();
                List<string> contacts = this.GetContacts();
                foreach (var c in contacts)
                {
                    missedCalls.Add(c, false);
                }
            }

            if (!IsAvailable())
            {
                return;
            }

            string command = "SEARCH MISSEDCALLS";
            this.QueryCommand(command, (responce) =>
            {
                SearchCallsResponce calls = responce as SearchCallsResponce;
                foreach (var id in calls.CallsIds)
                {
                    string command2 = String.Format("GET CALL {0} PARTNER_HANDLE", id);
                    this.QueryCommand(command2, (responce2) =>
                    {

                        CallPartnerHandleResponce handleResponce = responce2 as CallPartnerHandleResponce;
                        if (missedCalls.ContainsKey(handleResponce.PartnerHandle))
                        {
                            missedCalls[handleResponce.PartnerHandle] = true;
                        }
                    });
                }
            });

        }

        public void ResetMissedCalls()
        {

            if (!IsAvailable())
            {
                return;
            }

            string command = "SEARCH MISSEDCALLS";
            this.QueryCommand(command, (responce) =>
            {
                SearchCallsResponce calls = responce as SearchCallsResponce;
                foreach (var id in calls.CallsIds)
                {
                    string command2 = String.Format("SET CALL {0} SEEN", id);
                    this.QueryCommand(command2, (r) => { });
                }
            });

            List<string> contacts = this.GetContacts();

            foreach (var t in contacts)
            {
                missedCalls[t] = false;
            }
        }

        void timer2_Tick(object sender, EventArgs e)
        {
            UpdateContactsOnlinePrivate();
            UpdateMissedCalls();
        }

        void timer_Tick(object sender, EventArgs e)
        {
            //      lock (locker)
            {
                // lock (responces)
                {
                    Dictionary<string, Action<SkypeResponce>> actions2 = new Dictionary<string, Action<SkypeResponce>>();
                    Dictionary<string, SkypeResponce> responces2 = new Dictionary<string, SkypeResponce>();

                    foreach (var t in responces)
                    {
                        responces2.Add(t.Key, t.Value);
                    }
                    foreach (var t in actions)
                    {
                        actions2.Add(t.Key, t.Value);
                    }


                    foreach (var r in responces2)
                    {
                        var action = actions2[r.Key];
                        var param = r.Value;
                        /*Task.Factory.StartNew(() =>*/
                        action(param)/*)*/;
                        actions2.Remove(r.Key);
                    }

                    foreach (var t in responces2)
                    {
                        responces.Remove(t.Key);
                        actions.Remove(t.Key);
                    }

                    //   responces.Clear();
                }
            }
        }

        void skype_SkypeResponse(object theSender, SkypeResponseEventArgs theEventArgs)
        {
            string responceString = theEventArgs.Response;
            SkypeResponce responce = SkypeResponceParser.ParseResponce(responceString);
            if (responce is CommandResponce)
            {
                CommandResponce cr = responce as CommandResponce;
                responces.Add(cr.CommandId, cr.Responce);
                //   actions[cr.CommandId](cr.Responce);
                //   actions.Remove(cr.CommandId);
            }
        }

        public void QueryCommand(string command, Action<SkypeResponce> action)
        {
            string commandId = "#" + Helper.GetRandomString(7);
            actions.Add(commandId, action);
            skype.Command(commandId + " " + command);
        }

        Dictionary<string, bool> onlineContacts;

        //public List<string> GetOnlineContacts()
        //{
        //    if (onlineContacts == null)
        //    {
        //        return new List<string>();
        //    }

        //    List<string> res = new List<string>();
        //    foreach (var c in onlineContacts)
        //    {
        //        if (c.Value)
        //        {
        //            res.Add(c.Key);
        //        }
        //    }

        //    return res;
        //}
        Dictionary<string, bool> actualOnlineContacts;
        public Dictionary<string, bool> GetContactsStatuses()
        {
            if (actualOnlineContacts == null)
            {
                return new Dictionary<string, bool>();
            }

            return actualOnlineContacts;
        }

        public void Call(string skypeId)
        {
            if (!IsAvailable())
            {
                return;
            }

            state.Call(skypeId);
        }
        public void FinishCall()
        {
            if (!IsAvailable())
            {
                return;
            }

            state.FinishCall();
        }
        public void AnswerCall()
        {
            if (!IsAvailable())
            {
                return;
            }

            state.AnswerCall();
        }
        public void DenyCall()
        {
            if (!IsAvailable())
            {
                return;
            }

            state.DenyCall();
        }

        public event EventHandler CallRefused;
        public void OnCallRefused()
        {
            if (CallRefused != null)
            {
                CallRefused(this, new EventArgs());
            }
        }

        public event EventHandler CallFinished;
        public void OnCallFinished()
        {
            if (CallFinished != null)
            {
                CallFinished(this, new EventArgs());
            }
        }

        public event EventHandler CallMissed;
        public void OnCallMissed()
        {
            if (CallMissed != null)
            {
                CallMissed(this, new EventArgs());
            }
        }

        public event EventHandler<string> IncomingCall;
        public void OnIncomingCall(string partnerId)
        {
            if (IncomingCall != null)
            {
                IncomingCall(this, partnerId);
            }
        }

        public List<string> GetContacts()
        {
            string[] lines = System.IO.File.ReadAllLines(System.AppDomain.CurrentDomain.BaseDirectory + "skype.txt");
            List<string> res = new List<string>();
            foreach (var l in lines)
            {
                res.Add(l);
            }
            return res;
        }



    }
}
