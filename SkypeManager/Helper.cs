﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace SkypeManager
{
    class Helper
    {
        static Random random = new Random();

        public static string GetRandomString(int length)
        {
            string res = "";
            for (int i = 0; i < length; i++)
            {
                res += (char)('a' + random.Next(26));
            }
            return res;
        }

        public static bool IsInternetConnected()
        {
            try
            {
                TcpClient Tcp = new TcpClient("WWW.GMAIL.COM", 80);
                Tcp.Close();
                return true;
            }
            catch
            {
                return false;
            }
        }
    }
}
